#ifndef ORXPP_RENDER_H
#define ORXPP_RENDER_H

#include "orx.h"

#include "orxpp/viewport.h"
#include "orxpp/vector.h"

namespace orx {
namespace Render {

inline bool GetWorldPosition(const Vector& screenPosition,
                             const Viewport& viewport,
                             Vector& result)
{
    return orxRender_GetWorldPosition(&screenPosition.vector,
                                      viewport.viewport,
                                      &result.vector) != orxNULL;
}

inline bool GetWorldPosition(const Vector& screenPosition,
                             Vector& result)
{
    return orxRender_GetWorldPosition(&screenPosition.vector,
                                      orxNULL,
                                      &result.vector) != orxNULL;
}

inline Vector GetWorldPosition(const Vector& screenPosition, const Viewport& viewport = {})
{
    orxVECTOR result;
    orxRender_GetWorldPosition(&screenPosition.vector, viewport.viewport, &result);
    return &result;
}

} // namespace Render
} // namespace orx

#endif // ORXPP_RENDER_H
