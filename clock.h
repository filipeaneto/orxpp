#ifndef ORXPP_CLOCK_H
#define ORXPP_CLOCK_H

#include "orx.h"

#include "orxpp/string.h"
#include "orxpp/type.h"

#include <cstring>

namespace orx
{

// TODO
namespace Module
{
    enum class ID
    {
        Anim = orxMODULE_ID_ANIM,
        Main = orxMODULE_ID_MAIN
    };
}

class Clock
{
public:
    enum class Type : U32
    {
        Core = orxCLOCK_TYPE_CORE,
        User,
        Second,
        Number,
        None = orxENUM_NONE
    };

    enum class ModType : U32
    {
        Fixed = orxCLOCK_MOD_TYPE_FIXED,
        Multiply,
        Maxed,
        Number,
        None = orxENUM_NONE
    };

    struct Info
    {
        ModType modType;
        Type type;
        Float dt, modValue, tickSize, time;

        friend class Clock;

    protected:
        Info(const orxCLOCK_INFO *ci) :
            modType{static_cast<ModType>(ci->eModType)},
            type{static_cast<Type>(ci->eType)},
            dt{ci->fDT}, modValue{ci->fModValue},
            tickSize{ci->fTickSize}, time{ci->fTime} {}
    };

    enum class Priority : U32
    {
        Normal = orxCLOCK_PRIORITY_NORMAL
    };

    Clock() : clock{nullptr}, owner{true} {}

    Clock(ConstString configID) :
        clock{orxClock_CreateFromConfig(configID)}, owner{true} {}

    Clock(Clock&& source) : clock{source.clock}, owner{source.owner}
    {
        source.clock = nullptr;
    }

    Clock& operator=(Clock&& source)
    {
        if (owner && clock) orxClock_Delete(clock);

        clock = source.clock;
        owner = source.owner;

        source.clock = nullptr;

        return *this;
    }

    ~Clock()
    {
        if (owner && clock) orxClock_Delete(clock);
    }

    static Clock FindFirst(Float tickSize = orx2F(-1.0f), Type type = Type::Core)
    {
        return orxClock_FindFirst(tickSize, static_cast<orxCLOCK_TYPE>(type));
    }

    static Clock GetFromInfo(const Info& info)
    {
        orxCLOCK_INFO _info;

        static_assert(sizeof(orxCLOCK_INFO) == sizeof(Info), "invalid clock info size");
        std::memcpy(&_info, &info, sizeof(Info));

        return orxClock_GetFromInfo(&_info);
    }

    template<class C, class A>
    Status callback(A& context, Module::ID moduleID, Priority priority = Priority::Normal)
    {
        orxSTATUS result;

        result = orxClock_Register(clock, [](const orxCLOCK_INFO *ci, void *context){
            C::Update(Info{ci}, *static_cast<A*>(context));
        }, &context, static_cast<orxMODULE_ID>(moduleID), static_cast<orxCLOCK_PRIORITY>(priority));

        return static_cast<Status>(result);
    }

    template<class C>
    Status callback(Module::ID moduleID, Priority priority = Priority::Normal)
    {
        orxSTATUS result;

        result = orxClock_Register(clock, [](const orxCLOCK_INFO *ci, void *){
            C::Update(Info{ci});
        }, orxNULL, static_cast<orxMODULE_ID>(moduleID), static_cast<orxCLOCK_PRIORITY>(priority));

        return static_cast<Status>(result);
    }

    ConstString name() const
    {
        return orxClock_GetName(clock);
    }

    Status modifier(ModType modType, Float modValue = orx2F(0.0f))
    {
        return static_cast<Status>(orxClock_SetModifier(clock,
                    static_cast<orxCLOCK_MOD_TYPE>(modType), modValue));
    }

protected:
    Clock(orxCLOCK *source) : clock{source}, owner{false} {}

private:
    orxCLOCK *clock;
    bool owner;
};


}

#endif // ORXPP_CLOCK_H
