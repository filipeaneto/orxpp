#ifndef ORXPP_H
#define ORXPP_H

#include "orx.h"

#include "orxpp/aabox.h"
#include "orxpp/anim.h"
#include "orxpp/animpointer.h"
#include "orxpp/animset.h"
#include "orxpp/bank.h"
#include "orxpp/camera.h"
#include "orxpp/clock.h"
#include "orxpp/config.h"
#include "orxpp/display.h"
#include "orxpp/event.h"
#include "orxpp/frame.h"
#include "orxpp/input.h"
#include "orxpp/locale.h"
#include "orxpp/memory.h"
#include "orxpp/mouse.h"
#include "orxpp/object.h"
#include "orxpp/obox.h"
#include "orxpp/orxpp.h"
#include "orxpp/physics.h"
#include "orxpp/render.h"
#include "orxpp/shaderpointer.h"
#include "orxpp/sound.h"
#include "orxpp/soundsystem.h"
#include "orxpp/spawner.h"
#include "orxpp/string.h"
#include "orxpp/structure.h"
#include "orxpp/texture.h"
#include "orxpp/type.h"
#include "orxpp/vector.h"
#include "orxpp/viewport.h"

namespace orx
{

template<class Game>
inline void Execute(int argc, char *argv[])
{
    orx_Execute(argc, argv,
                [](){ return static_cast<orxSTATUS>(Game::Init()); },
                [](){ return static_cast<orxSTATUS>(Game::Run()); },
                Game::Exit);
}

template<class T, class... I>
inline T Create(I... input) { return T::Create(input...); }

template<class T>
inline Status Delete(T& t) { return T::Delete(t); }

}

#endif // ORXPP_H
