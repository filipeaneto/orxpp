ifndef config
  config=debug64
endif

CXX = g++
AR = ar
CFLAGS = -Wall

ifeq ($(config),debug64)
  OBJDIR    = obj/Debug/x64
  TARGETDIR = lib/x64
  TARGET    = $(TARGETDIR)/liborxppd.a
  DEFINES  += -D__orxDEBUG__
  INCLUDES += -Iinclude -I/usr/local/include/orx
  ALL_CPPFLAGS  += $(CPPFLAGS) -MMD -MP $(DEFINES) $(INCLUDES)
  ALL_CFLAGS    += $(CFLAGS) $(ALL_CPPFLAGS) $(ARCH) -msse2 -ffast-math -g -m64
  ALL_CXXFLAGS  += $(CXXFLAGS) $(ALL_CFLAGS) -std=c++11 -fno-exceptions
  ALL_RESFLAGS  += $(RESFLAGS) $(DEFINES) $(INCLUDES)
  ALL_LDFLAGS   += $(LDFLAGS) -L/usr/local/lib -m64 # -Wl,-rpath ./ -Wl,--export-dynamic
  LIBS          += -lorxd -ldl -lm -lrt
  LINKCMD    = $(CC) -o $(TARGET) $(OBJECTS) $(RESOURCES) $(ARCH) $(ALL_LDFLAGS) $(LIBS)
  ARCMD     = $(AR) rvs $(TARGET) $(OBJECTS)
endif

ifeq ($(config),release64)
  OBJDIR    = obj/Release/x64
  TARGETDIR = lib/x64
  TARGET    = $(TARGETDIR)/liborxpp.a
  INCLUDES += -Iinclude -I/usr/local/include/orx
  ALL_CPPFLAGS  += $(CPPFLAGS) -MMD -MP $(DEFINES) $(INCLUDES)
  ALL_CFLAGS    += $(CFLAGS) $(ALL_CPPFLAGS) $(ARCH) -msse2 -ffast-math -g -O2 -m64 -fschedule-insns
  ALL_CXXFLAGS  += $(CXXFLAGS) $(ALL_CFLAGS) -std=c++11 -fno-exceptions -fno-rtti
  ALL_RESFLAGS  += $(RESFLAGS) $(DEFINES) $(INCLUDES)
  ALL_LDFLAGS   += $(LDFLAGS) -L/usr/local/lib -m64 # -Wl,-rpath ./ -Wl,--export-dynamic
  LIBS          += -lorxd -ldl -lm -lrt
  LINKCMD    = $(CC) -o $(TARGET) $(OBJECTS) $(RESOURCES) $(ARCH) $(ALL_LDFLAGS) $(LIBS)
  ARCMD     = $(AR) rvs $(TARGET) $(OBJECTS)
endif

SRCDIR = src
SOURCES := $(shell ls $(SRCDIR)/*.cpp)

OBJECTS := $(subst $(SRCDIR),$(OBJDIR),$(SOURCES:%.cpp=%.o))

.PHONY: clean

all: $(OBJDIR) $(TARGETDIR) $(TARGET)

$(TARGET): $(OBJECTS) $(LDDEPS)
	$(ARCMD)

$(OBJDIR):
	mkdir -p $(OBJDIR)

$(TARGETDIR):
	mkdir -p $(TARGETDIR)

clean:
	rm -f $(TARGET)
	rm -rf $(OBJDIR)/*

#$(OBJDIR)/%.gch: $(SRCDIR)/%.cpp
#	$(CXX) $(ALL_CXXFLAGS) -o $@ -MM $<
#	$(CXX) -x c-header $(ALL_CXXFLAGS) -MMD -MP $(DEFINES) $(INCLUDES) -o "$@" -MF "$(@:%.gch=%.d)" -c "$<"

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(ALL_CXXFLAGS) $(FORCE_INCLUDE) -o "$@" -MF $(@:%.o=%.d) -c "$<"

-include $(OBJECTS:%.o=%.d)
