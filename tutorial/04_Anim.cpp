#include "orxpp/orxpp.h"

using namespace orx;

class Game
{
public:
    Game() : viewport{"Viewport"}, soldier{"Soldier"} {}

    static Status orxFASTCALL EventHandler(const Anim::Event& event) {
        using ID = Anim::Event::ID;

        switch (event.id())
        {
        case ID::Start:
            orxLOG("Animation <%s>@<%s> has started!",
                   event.animName(), event.recipient().name());
            break;
        case ID::Stop:
            orxLOG("Animation <%s>@<%s> has stopped!",
                   event.animName(), event.recipient().name());
            break;
        case ID::Cut:
            orxLOG("Animation <%s>@<%s> has been cut!",
                   event.animName(), event.recipient().name());
            break;
        case ID::Loop:
            orxLOG("Animation <%s>@<%s> has looped!",
                   event.animName(), event.recipient().name());
            break;
        case ID::CustomEvent:
            orxLOG("Animation <%s>@<%s> has sent the event [%s]!",
                   event.animName(), event.recipient().name(),
                   event.customEventName());
            break;
        }

        return Status::Success;
    }

    static void orxFASTCALL Update(const Clock::Info& clockInfo) {
        Object& soldier = Instance->soldier;

        if (Input::IsActive("GoRight"))
            soldier.targetAnim("WalkRight");
        else if (Input::IsActive("GoLeft"))
            soldier.targetAnim("WalkLeft");
        else
            soldier.targetAnim(nullptr);

        if (Input::IsActive("ScaleUp"))
            soldier.scale(soldier.scale() * orx2F(1.02f));

        if (Input::IsActive("ScaleDown"))
            soldier.scale(soldier.scale() * orx2F(0.98f));
    }

    static Status orxFASTCALL Init() {
        if (!Instance) Instance = new Game;

        Clock::FindFirst(-1.0f, Clock::Type::Core).callback<Game>(Module::ID::Main);
        Event::AddHandler<Game, Anim::Event>();

        return Status::Success;
    }

    static Status orxFASTCALL Run() {
        return Input::IsActive("Quit") ? Status::Failure : Status::Success;
    }

    static void orxFASTCALL Exit() {
        delete Instance;
        Instance = nullptr;
    }

private:
    static Game *Instance;

    Viewport viewport;
    Object soldier;
};

Game *Game::Instance{nullptr};

int main(int argc, char **argv)
{
    Execute<Game>(argc, argv);

    return EXIT_SUCCESS;
}
