#include "orxpp/orxpp.h"

using namespace orx;

class Game
{
public:
    Game() :
        viewport{"Viewport1"}, viewport2{"Viewport2"},
        viewport3{"Viewport3"}, viewport4{"Viewport4"},
        box{"Box"}, soldier{"Soldier"} {}

    static void orxFASTCALL Update(const Clock::Info& clockInfo) {
        Vector pos, sPos;
        Float2 f;
        Camera camera{Instance->viewport.camera()};

        if (Input::IsActive("CameraRotateLeft"))
            camera.rotation(camera.rotation() + orx2F(-4.0f) * clockInfo.dt);

        if (Input::IsActive("CameraRotateRight"))
            camera.rotation(camera.rotation() + orx2F(4.0f) * clockInfo.dt);

        if (Input::IsActive("CameraZoomIn"))
            camera.zoom(camera.zoom() * orx2F(1.02f));

        if (Input::IsActive("CameraZoomOut"))
            camera.zoom(camera.zoom() * orx2F(0.98f));

        pos = camera.position();

        if (Input::IsActive("CameraRight"))
            pos.x() += orx2F(500) * clockInfo.dt;

        if (Input::IsActive("CameraLeft"))
            pos.x() -= orx2F(500) * clockInfo.dt;

        if (Input::IsActive("CameraDown"))
            pos.y() += orx2F(500) * clockInfo.dt;

        if (Input::IsActive("CameraUp"))
            pos.y() -= orx2F(500) * clockInfo.dt;

        camera.position(pos);

        f = Instance->viewport.relativeSize();

        if (Input::IsActive("ViewportScaleUp")) {
            f.width *= orx2F(1.02f);
            f.height *= orx2F(1.02f);
        }

        if (Input::IsActive("ViewportScaleDown")) {
            f.width *= orx2F(0.98f);
            f.height *= orx2F(0.98f);
        }

        Instance->viewport.relativeSize(f.width, f.height);

        f = Instance->viewport.position();

        if (Input::IsActive("ViewportRight")) f.x += orx2F(500) * clockInfo.dt;
        if (Input::IsActive("ViewportLeft"))  f.x -= orx2F(500) * clockInfo.dt;
        if (Input::IsActive("ViewportDown"))  f.y += orx2F(500) * clockInfo.dt;
        if (Input::IsActive("ViewportUp"))    f.y -= orx2F(500) * clockInfo.dt;

        Instance->viewport.position(f.x, f.y);

        pos = Render::GetWorldPosition(Mouse::GetPosition());
        sPos = Instance->soldier.worldPosition();

        pos.z() = sPos.z();
        Instance->soldier.position(pos);
    }

    static Status orxFASTCALL Init() {
        if (!Instance) Instance = new Game;

        Clock::FindFirst().callback<Game>(Module::ID::Main);

        return Status::Success;
    }

    static Status orxFASTCALL Run() {
        return Input::IsActive("Quit") ? Status::Failure : Status::Success;
    }

    static void orxFASTCALL Exit() {
        delete Instance;
        Instance = nullptr;
    }

private:
    static Game *Instance;

    Viewport viewport, viewport2, viewport3, viewport4;
    Object box, soldier;
};

Game *Game::Instance{nullptr};

int main(int argc, char **argv)
{
    Execute<Game>(argc, argv);

    return EXIT_SUCCESS;
}
