#include "orxpp/orxpp.h"

using namespace orx;

class Game
{
public:
    Game() :
        viewport{"Viewport"}, parentObject{"ParentObject"},
        object0{"Object0"}, object1{"Object1"}, object2{"Object2"}
    {
        object1.parent(parentObject);
        object2.parent(parentObject);
    }

    static void orxFASTCALL Update(const Clock::Info& clockInfo) {
        Object& parent = Instance->parentObject;

        if (Input::IsActive("RotateLeft"))
            parent.rotation(parent.rotation() - orxMATH_KF_PI * clockInfo.dt);

        if (Input::IsActive("RotateRight"))
            parent.rotation(parent.rotation() + orxMATH_KF_PI * clockInfo.dt);

        if (Input::IsActive("ScaleUp"))
            parent.scale(parent.scale() * orx2F(1.02f));

        if (Input::IsActive("ScaleDown"))
            parent.scale(parent.scale() * orx2F(0.98f));

        Vector position;
        if (Render::GetWorldPosition(Mouse::GetPosition(), position)) {
            position.z() = parent.worldPosition().z();
            parent.position(position);
        }
    }

    static Status orxFASTCALL Init() {
        if (!Instance) Instance = new Game;

        Clock clock{Clock::FindFirst(-1.0f, Clock::Type::Core)};
        clock.callback<Game>(Module::ID::Main, Clock::Priority::Normal);

        return Status::Success;
    }

    static Status orxFASTCALL Run() {
        return Input::IsActive("Quit") ? Status::Failure : Status::Success;
    }

    static void orxFASTCALL Exit() {
        delete Instance;
        Instance = nullptr;
    }

private:
    static Game *Instance;

    Viewport viewport;
    Object parentObject;
    Object object0, object1, object2;
};

Game *Game::Instance{nullptr};

int main(int argc, char **argv)
{
    orx::Execute<Game>(argc, argv);

    return EXIT_SUCCESS;
}
