#include "orxpp/orxpp.h"

using namespace orx;

class Game {
public:
    static Status orxFASTCALL EventHandler(const Sound::Event& event) {
        using ID = Sound::Event::ID;

        switch (event.id())
        {
        case ID::Start:
            orxLOG("Sound <%s>@<%s> has started!",
                   event.soundName(), event.recipient().name());
            break;

        case ID::Stop:
            orxLOG("Sound <%s>@<%s> has stopped!",
                   event.soundName(), event.recipient().name());
            break;
        }

        return Status::Success;
    }

    static void orxFASTCALL Update(const Clock::Info& clockInfo) {
        if (Input::IsActive("RandomSFX") && Input::HasNewStatus("RandomSFX")) {
            soldier.addSound("RandomBip");

            Config::Section("Tutorial", [&](){
                soldier.color(Config::GetVector("RandomColor"));
            });
        }

        if (Input::IsActive("DefaultSFX") && Input::HasNewStatus("DefaultSFX")) {
            soldier.addSound("DefaultBip");
            soldier.color(Vector::White);
        }

        if (Input::IsActive("ToggleMusic") && Input::HasNewStatus("ToggleMusic")) {
            if (music.status() != Sound::Status::Play) {
                music.play();
                soldier.enable();
            } else {
                music.pause();
                soldier.enable(false);
            }
        }

        if (Input::IsActive("PitchUp")) {
            music.pitch(music.pitch() + orx2F(0.01f));
            soldier.rotation(soldier.rotation() + orx2F(4.0f) * clockInfo.dt);
        }

        if (Input::IsActive("PitchDown")) {
            music.pitch(music.pitch() - orx2F(0.01f));
            soldier.rotation(soldier.rotation() - orx2F(4.0f) * clockInfo.dt);
        }

        if (Input::IsActive("VolumeDown")) {
            music.volume(music.volume() - orx2F(0.05f));
            soldier.scale(soldier.scale() * 0.98f);
        }

        if (Input::IsActive("VolumeUp")) {
            music.volume(music.volume() + orx2F(0.05f));
            soldier.scale(soldier.scale() * 1.02f);
        }
    }

    static Status orxFASTCALL Init() {
        Input::Type type;
        Enum id;

        ConstString inputVolumeUp;
        ConstString inputVolumeDown;
        ConstString inputPitchUp;
        ConstString inputPitchDown;
        ConstString inputToggleMusic;
        ConstString inputRandomSFX;
        ConstString inputDefaultSFX;

        Input::GetBinding("VolumeUp", 0, type, id);
        inputVolumeUp = Input::GetBindingName(type, id);

        Input::GetBinding("VolumeDown", 0, type, id);
        inputVolumeDown = Input::GetBindingName(type, id);

        Input::GetBinding("PitchUp", 0, type, id);
        inputPitchUp = Input::GetBindingName(type, id);

        Input::GetBinding("PitchDown", 0, type, id);
        inputPitchDown = Input::GetBindingName(type, id);

        Input::GetBinding("ToggleMusic", 0, type, id);
        inputToggleMusic = Input::GetBindingName(type, id);

        Input::GetBinding("RandomSFX", 0, type, id);
        inputRandomSFX = Input::GetBindingName(type, id);

        Input::GetBinding("DefaultSFX", 0, type, id);
        inputDefaultSFX = Input::GetBindingName(type, id);

        orxLOG("\n- '%s' & '%s' will change the music volume (+ soldier size)"
               "\n- '%s' & '%s' will change the music pitch (+ soldier rotation)"
               "\n- '%s' will toggle music (+ soldier display)"
               "\n- '%s' will play a random SFX on the soldier (+ change its color)"
               "\n- '%s' will the default SFX on the soldier (+ restore its color)"
               "\n! The sound effect will be played only if the soldier is active",
               inputVolumeUp, inputVolumeDown, inputPitchUp, inputPitchDown,
               inputToggleMusic, inputRandomSFX, inputDefaultSFX);

        viewport = Create<Viewport>("Viewport");
        soldier = Create<Object>("Soldier");

        music = Create<Sound>("Music");
        music.play();

        Clock::FindFirst().callback<Game>(Module::ID::Main);
        Event::AddHandler<Game, Sound::Event>();

        return Status::Success;
    }

    static Status orxFASTCALL Run() {
        return Input::IsActive("Quit") ? Status::Failure : Status::Success;
    }

    static void orxFASTCALL Exit() {
        Delete(music);
        Delete(soldier);
        Delete(viewport);
    }

private:
    static Viewport viewport;
    static Object soldier;
    static Sound music;
};

Viewport Game::viewport{};
Object Game::soldier{};
Sound Game::music{};

int main(int argc, char **argv)
{
    Execute<Game>(argc, argv);

    return EXIT_SUCCESS;
}
