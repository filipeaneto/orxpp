#include "orxpp/orxpp.h"

using namespace orx;

class Game {
public:
    Game() :
        viewport{"Viewport"},
        object1{"Object1"}, object2{"Object2"},
        clock1{"Clock1"}, clock2{"Clock2"}
    {
        clock1.callback<Game>(object1, Module::ID::Main, Clock::Priority::Normal);
        clock2.callback<Game>(object2, Module::ID::Main, Clock::Priority::Normal);
    }

    static void orxFASTCALL Update(const Clock::Info& clockInfo, Object& object) {
        bool displayLog;
        Config::Section("Main", [&](){ displayLog = Config::GetBool("DisplayLog"); });

        if (displayLog) orxLOG("<%s>: Time = %.3f / DT = %.3f",
                               Clock::GetFromInfo(clockInfo).name(),
                               clockInfo.time, clockInfo.dt);

        object.rotation(orxMATH_KF_PI * clockInfo.time);
    }

    static void orxFASTCALL Update(const Clock::Info& clockInfo) {
        if (Input::IsActive("Log") && Input::HasNewStatus("Log"))
            Config::Section("Main", [&]() {
                Config::Set("DisplayLog", !Config::GetBool("DisplayLog"));
            });

        Clock clock{Clock::FindFirst(-1.0f, Clock::Type::User)};

        if (Input::IsActive("Faster"))
            clock.modifier(Clock::ModType::Multiply, 4.0f);
        else if (Input::IsActive("Slower"))
            clock.modifier(Clock::ModType::Multiply, 0.25f);
        else if (Input::IsActive("Normal"))
            clock.modifier(Clock::ModType::None);
    }

    static Status orxFASTCALL Init() {
        ConstString inputLog, inputFaster, inputSlower, inputNormal;
        Input::Type type;
        Enum id;

        Input::GetBinding("Log", 0, type, id);
        inputLog = Input::GetBindingName(type, id);

        Input::GetBinding("Faster", 0, type, id);
        inputFaster = Input::GetBindingName(type, id);

        Input::GetBinding("Slower", 0, type, id);
        inputSlower = Input::GetBindingName(type, id);

        Input::GetBinding("Normal", 0, type, id);
        inputNormal = Input::GetBindingName(type, id);

        orxLOG("\n- Press '%s' to toggle log display"
               "\n- To stretch time for the first clock (updating the box):"
               "\n . Press numpad '%s' to set it 4 times faster"
               "\n . Press numpad '%s' to set it 4 times slower"
               "\n . Press numpad '%s' to set it back to normal",
               inputLog, inputFaster, inputSlower, inputNormal);

        if (!Instance) Instance = new Game;

        Clock mainClock{Clock::FindFirst(-1.0f, Clock::Type::Core)};
        mainClock.callback<Game>(Module::ID::Main, Clock::Priority::Normal);

        return Status::Success;
    }

    static Status orxFASTCALL Run() {
        return Input::IsActive("Quit") ? Status::Failure : Status::Success;
    }

    static void orxFASTCALL Exit() {
        delete Instance;
        Instance = nullptr;
    }

private:
    static Game *Instance;

    Viewport viewport;
    Object object1, object2;
    Clock clock1, clock2;
};

Game *Game::Instance{nullptr};

int main(int argc, char **argv)
{
    Execute<Game>(argc, argv);

    return EXIT_SUCCESS;
}
