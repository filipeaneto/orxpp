#include "orxpp/orxpp.h"

using namespace orx;

class Game
{
public:
    static Status orxFASTCALL Init() {
        orxLOG("\n* This tutorial creates a viewport/camera couple and an object"
               "\n* You can play with the config parameters in ../01_Object.ini"
               "\n* After changing them, relaunch the tutorial to see their effects");

        viewport = Create<Viewport>("Viewport");
        object = Object::Create("Object");

        return Status::Success;
    }

    static Status orxFASTCALL Run() {
        return Input::IsActive("Quit") ? Status::Failure : Status::Success;
    }

    static void orxFASTCALL Exit() {
        Delete(object);
        Viewport::Delete(viewport);
    }

private:
    static Viewport viewport;
    static Object object;
};

Viewport Game::viewport{};
Object Game::object{};

int main(int argc, char **argv)
{
    Execute<Game>(argc, argv);

    return EXIT_SUCCESS;
}
