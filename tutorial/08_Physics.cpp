#include "orxpp/orxpp.h"

#include <vector>

using namespace orx;

class Game
{
public:
    Game() : viewport{"Viewport"}, camera{viewport.camera()}, sky{"Sky"}, walls{"Walls"} {
        U32 boxNumber;
        Config::Section("Tutorial", [&](){ boxNumber = Config::GetU32("BoxNumber"); });

        for (U32 i{}; i < boxNumber; ++i)
            boxes.push_back(Object{"Box"});
    }

    static Status orxFASTCALL EventHandler(const Physics::Event &event) {
        if (event.id == Physics::Event::ID::ContactAdd) {
            event.recipient.addFX("Bump");
            event.sender.addFX("Bump");
        }

        return Status::Success;
    }

    static void orxFASTCALL Update(const Clock::Info &clockInfo) {
        Float deltaRotation = 0.0f;

        if (Input::IsActive("RotateLeft")) deltaRotation = 4.0f * clockInfo.dt;
        if (Input::IsActive("RotateRight")) deltaRotation = -4.0f * clockInfo.dt;

        if (deltaRotation != 0.0f) {
            Instance->camera.rotation(Instance->camera.rotation() + deltaRotation);

            Vector gravity = Vector::Rotate2D(Physics::GetGravity(), deltaRotation);
            Physics::SetGravity(gravity);
        }

    }

    static Status orxFASTCALL Init() {
        if (!Instance) Instance = new Game;

        Clock clock{Clock::FindFirst(-1.0f, Clock::Type::Core)};
        clock.callback<Game>(Module::ID::Main, Clock::Priority::Normal);

        Event::AddHandler<Game, Physics::Event>();

        return Status::Success;
    }

    static Status orxFASTCALL Run() {
        return Input::IsActive("Quit") ? Status::Failure : Status:: Success;
    }

    static void orxFASTCALL Exit() {}

private:
    static Game *Instance;

    Viewport viewport;
    Camera camera;
    Object sky, walls;
    std::vector<Object> boxes;
};

Game *Game::Instance{nullptr};

int main(int argc, char **argv)
{
    Execute<Game>(argc, argv);

    return EXIT_SUCCESS;
}
