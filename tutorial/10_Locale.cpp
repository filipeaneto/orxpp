#include "orxpp/orxpp.h"
#include <iostream>

using namespace orx;

class Logo : private Object
{
public:
    Logo() : Object{"Logo"}, legend{"Legend"} {
        userData(*this);
    }

private:
    Object legend;
};

class Game
{
public:
    Game() : viewport{"Viewport"}, logo{}, languageIndex{0} {
        Event::AddHandler<Game, Locale::Event>();

        std::cout << "The available languages are:" << std::endl;
        for (U32 i{}; i != Locale::GetLanguageCounter(); ++i)
            std::cout << " - " << Locale::GetLanguage(i) << std::endl;
    }

    static Status orxFASTCALL EventHandler(const Locale::Event& event) {
        using ID = Locale::Event::ID;

        switch (event.id())
        {
        case ID::SelectLanguage:
            orxLOG("Switching to '%s'.", event.language());
            break;
        }

        return Status::Success;
    }

    static Status orxFASTCALL Init() {
        Input::Type type;
        Enum id;

        ConstString inputQuit;
        ConstString inputCycle;

        Input::GetBinding("Quit", 0, type, id);
        inputQuit = Input::GetBindingName(type, id);

        Input::GetBinding("CycleLanguage", 0, type, id);
        inputCycle = Input::GetBindingName(type, id);

        orxLOG("\n- '%s' will exit from this tutorial"
               "\n- '%s' will cycle through all the available languages"
               "\n* The legend under the logo is always displayed in the current language",
               inputQuit, inputCycle);

        orxLOG("10_Locale Init() called!");

        if (!Instance) Instance = new Game;

        return Status::Success;
    }

    static Status orxFASTCALL Run() {
        if (Input::IsActive("Quit")) {
            orxLOG("Quit action triggered, exiting!");
            return Status::Failure;
        }

        if (Input::IsActive("CycleLanguage") && Input::HasNewStatus("CycleLanguage"))
            Instance->selectNextLanguage();

        return Status::Success;
    }

    static void   orxFASTCALL Exit() {
        delete Instance;
        Instance = nullptr;

        orxLOG("10_Locale Exit() called!");
    }

    void selectNextLanguage() {
        languageIndex = (languageIndex == Locale::GetLanguageCounter() - 1) ?
            0 : languageIndex + 1;

        Locale::SelectLanguage(Locale::GetLanguage(languageIndex));
    }

private:
    static Game *Instance;

    Viewport viewport;
    Logo logo;
    U32 languageIndex;
};

Game *Game::Instance{nullptr};

int main(int argc, char **argv)
{
    Execute<Game>(argc, argv);

    return EXIT_SUCCESS;
}
