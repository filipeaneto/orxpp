#ifndef ORXPP_TYPE_H
#define ORXPP_TYPE_H

#include <orx.h>

namespace orx
{

using S64 = orxS64;
using S32 = orxS32;
using S16 = orxS16;
using S8  = orxS8;

using U64 = orxU64;
using U32 = orxU32;
using U16 = orxU16;
using U8  = orxU8;

using Float = orxFLOAT;
using Double = orxDOUBLE;

struct Float2
{
    union {
        Float x;
        Float width;
    };
    union {
        Float y;
        Float height;
    };
};

struct Bool2
{
    bool x;
    bool y;
};


class Status
{
public:
    enum ID : U32
    {
        Failure = 0, Success, Number, None = orxENUM_NONE
    };

    Status(orxSTATUS value = {});
    Status(ID value);

    bool operator==(const Status& other) const;
    bool operator!=(const Status& other) const;

    operator orxSTATUS() const;
    operator bool() const;
    operator ID() const;

private:
    orxSTATUS status;
};

}

#endif // ORXPP_TYPE_H
