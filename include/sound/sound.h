#ifndef ORXPP_SOUND_H
#define ORXPP_SOUND_H

#include <orx.h>

#include "core/event.h"
#include "base/type.h"

#include <cstddef>

namespace orx
{

class ConstString;
class Object;
class Vector;

namespace SoundSystem {
    class Sample;
}

class Sound
{
public:
    class Event : private orx::Event
    {
    public:
        static constexpr orx::Event::Type EventType{orx::Event::Type::Sound};

        enum class ID : U32
        {
            Start = 0, Stop, Packet, RecordingStart, RecordingStop, RecordingPacket,
            Number, None = orxENUM_NONE
        };

        Event(const orx::Event& source);

        ID id() const;
        Object recipient() const;

        Sound sound() const;

        // Stream Info
        U32 channelNumber() const;
        U32 sampleRate() const;

        // Packet
        S16 *sampleList() const;
        void sampleList(S16 *sl) const;

        U32 sampleNumber() const;
        void sampleNumber(U32 sn) const;

        bool discard() const;
        void discard(bool d) const;

        Float timeStamp() const;
        S32 packetID() const;

        ConstString soundName() const;
    };

    enum class Status : U32
    {
        Play = 0, Pause, Stop, Number, None = orxENUM_NONE
    };

    // Creators, destructor and assignment
    Sound();
    explicit Sound(std::nullptr_t);
    explicit Sound(ConstString configID);
    explicit Sound(U32 channelNumber, U32 sampleRate, ConstString name);
    Sound(const Sound& source) = delete;
    Sound(Sound&& source);

    Sound& operator=(const Sound& source) = delete;
    Sound& operator=(Sound&& source);

    virtual ~Sound();

    // Validity and equality check
    operator bool() const;
    bool operator==(const Sound& other) const;
    bool operator!=(const Sound& other) const;

    // Internal module function
    static void   Setup();
    static orx::Status Init();
    static void   Exit();

    // Basic handling
    static Sound Create(std::nullptr_t);
    static Sound Create(ConstString configID);
    static Sound Create(U32 channelNumber, U32 sampleRate, ConstString name);

    static SoundSystem::Sample
        CreateSample(U32 channelNumber, U32 frameNumber, U32 sampleRate, ConstString name);

    static orx::Status Delete(Sound& sound);
    static orx::Status DeleteSample(ConstString sampleName);

    // Methods
    orx::Status attenuation(Float a);
    Float attenuation() const;

    Float duration() const;

    orx::Status pitch(Float p);
    Float pitch() const;

    orx::Status volume(Float v);
    Float volume() const;

    ConstString name() const;

    orx::Status position(const Vector& p);
    Vector position() const;

    orx::Status referenceDistance(Float distance);
    Float referenceDistance() const;

    Sound::Status status() const;

    bool looping() const;
    orx::Status loop(bool l = true);

    orx::Status pause();
    orx::Status play();
    orx::Status stop();

    bool isStream();

    orx::Status linkSample(ConstString sampleName);
    orx::Status unlinkSample();

    static SoundSystem::Sample GetSample(ConstString name);

    static bool HasRecordingSupport();

    static orx::Status StartRecording(const char *filename, bool write = true,
                                             U32 sampleRate = 0, U32 channelNumber = 0);
    static orx::Status StopRecording();

protected:
    Sound(orxSOUND *source);

private:
    orxSOUND *sound;
    bool owns;
};

}

#endif // ORXPP_SOUND_H
