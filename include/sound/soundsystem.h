#ifndef ORXPP_SOUNDSYSTEM_H
#define ORXPP_SOUNDSYSTEM_H

#include <orx.h>

#include "base/type.h"

namespace orx
{

class ConstString;
class Vector;
class Sound;

namespace SoundSystem
{

enum class Status : U32
{
    Play = 0, Pause, Stop, Number, None = orxENUM_NONE
};

class Sound;

class Sample
{
public:
    // Creators, destructor and assignment
    Sample();
    explicit Sample(U32 channelNumber, U32 frameNumber, U32 sampleRate);
    Sample(const Sample& source) = delete;
    Sample(Sample&& source);

    Sample& operator=(const Sample& source) = delete;
    Sample& operator=(Sample&& source);

    virtual ~Sample();

    // Validity and equality check
    operator bool() const;
    bool operator==(const Sample& other);
    bool operator!=(const Sample& other);

    // Basic handling
    static Sample Create(U32 channelNumber, U32 frameNumber, U32 sampleRate);
    static orx::Status Delete(Sample& sample);

    // Methods
    orx::Status info(U32& channelNumber, U32& frameNumber, U32& sampleRate) const;
    orx::Status sampleData(const S16 *data, U32 sampleNumber);

    // Friendship
    friend class orx::Sound;
    friend class orx::SoundSystem::Sound;

protected:
    Sample(orxSOUNDSYSTEM_SAMPLE *source);

private:
    orxSOUNDSYSTEM_SAMPLE *sample;
    bool owns;
};

class Sound
{
public:
    // Creators, destructor and assignment
    Sound();
    explicit Sound(const Sample& sample);
    explicit Sound(U32 channelNumber, U32 sampleRate, ConstString reference);
    Sound(const Sound& source) = delete;
    Sound(Sound&& source);

    Sound& operator=(const Sound& source) = delete;
    Sound& operator=(Sound&& source);

    virtual ~Sound();

    // Validity and equality check
    operator bool() const;
    bool operator==(const Sound& other);
    bool operator!=(const Sound& other);

    // Basic handling
    static Sound Create(const Sample& sample);
    static Sound Create(U32 channelNumber, U32 sampleRate, ConstString reference);
    static orx::Status Delete(Sound& sound);

    // Methods
    orx::Status attenuation(Float a);
    Float attenuation() const;

    Float duration() const;

    orx::Status pitch(Float p);
    Float pitch() const;

    orx::Status volume(Float v);
    Float volume() const;

    orx::Status position(const Vector& p);
    Vector position() const;

    orx::Status referenceDistance(Float distance);
    Float referenceDistance() const;

    SoundSystem::Status status() const;

    bool looping() const;
    orx::Status loop(bool l = true);

    orx::Status pause();
    orx::Status play();
    orx::Status stop();

protected:
    Sound(orxSOUNDSYSTEM_SOUND *source);

private:
    orxSOUNDSYSTEM_SOUND *sound;
    bool owns;
};

orx::Status Init();
void Setup();
void Exit();

orx::Status SetGlobalVolume(Float volume);
Float GetGlobalVolume();

orx::Status SetListenerPosition(const Vector& position);
Vector GetListenerPosition();

bool HasRecordingSupport();

orx::Status StartRecording(const char *filename, bool write = true,
                                  U32 sampleRate = 0, U32 channelNumber = 0);
orx::Status StopRecording();

}

}

#endif // ORXPP_SOUNDSYSTEM_H
