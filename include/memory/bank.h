#ifndef ORXPP_BANK_H
#define ORXPP_BANK_H

#include <orx.h>

#include "memory/bank_decl.h"

#include "memory/memory.h"
#include "base/type.h"

namespace orx
{

template<class T>
Bank<T>::Bank() : bank{nullptr}, owns{true} {}

template<class T>
Bank<T>::Bank(U16 nbElem, U32 flags, Memory::Type memType) :
    bank{orxBank_Create(nbElem, sizeof(T), flags, static_cast<orxMEMORY_TYPE>(memType))},
    owns{true} {}

template<class T>
Bank<T>::Bank(Bank&& source) : bank{source.bank}, owns{source.owns}
{
    source.bank = nullptr;
}

template<class T>
Bank<T>& Bank<T>::operator=(Bank&& source)
{
    if (owns && bank) orxBank_Delete(bank);

    bank = source.bank;
    owns = source.owns;

    source.bank = nullptr;

    return *this;
}

template<class T>
Bank<T>::Bank(orxBANK *source) : bank{source}, owns{false} {}

template<class T>
Bank<T>::~Bank()
{
    if (owns && bank) orxBank_Delete(bank);
}

template<class T>
Bank<T>::operator bool() const { return bank != nullptr; }

template<class T>
bool Bank<T>::operator==(const Bank& other) const
{
    return bank && bank == other.bank;
}

template<class T>
bool Bank<T>::operator!=(const Bank& other) const
{
    return bank && bank != other.bank;
}

Status Bank<>::Init() { return orxBank_Init(); }

void Bank<>::Setup() { orxBank_Setup(); }

void Bank<>::Exit() { orxBank_Exit(); }

template<class T>
Bank<T> Bank<T>::Create(U16 nbElem, U32 flags, Memory::Type memType)
{
    return orxBank_Create(nbElem, sizeof(T), flags, static_cast<orxMEMORY_TYPE>(memType));
}

template<class T>
Status Bank<T>::Delete(Bank<T>& bank) { return orxBank_Delete(bank); }

void Bank<>::CompactAll() { orxBank_CompactAll(); }

template<class T>
T *Bank<T>::allocate()
{
    return static_cast<T*>(orxBank_Allocate(bank));
}

template<class T>
T *Bank<T>::allocate(U32& index)
{
    return static_cast<T*>(orxBank_AllocateIndexed(bank, &index));
}

template<class T>
void Bank<T>::clear() { orxBank_Clear(bank); }

template<class T>
void Bank<T>::compact() { orxBank_Compact(bank); }

template<class T>
void Bank<T>::free(T *cell) { orxBank_Free(bank, cell); }

template<class T>
T *Bank<T>::at(U32 index) const
{
    return static_cast<T*>(orxBank_GetAtIndex(bank, index));
}

template<class T>
T *Bank<T>::operator[](U32 index) const { return at(index); }

template<class T>
U32 Bank<T>::counter() const { return orxBank_GetCounter(bank); }

template<class T>
U32 Bank<T>::index(const T *cell) const
{
    return orxBank_GetIndex(bank, cell);
}

template<class T>
T *Bank<T>::next(const T *cell) const
{
    return static_cast<T*>(orxBank_GetNext(bank, cell));
}

}

#endif // ORXPP_BANK_H
