#ifndef ORXPP_MEMORY_H
#define ORXPP_MEMORY_H

#include <base/type.h>

namespace orx
{

namespace Memory
{

enum class Type : U32
{
    Main = 0, Video, Config, Text, Audio, Physics, Temp, Number, None = orxENUM_NONE
};

}

}
#endif // ORXPP_MEMORY_H
