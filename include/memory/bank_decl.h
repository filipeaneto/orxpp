#ifndef ORXPP_BANK_DECL_H
#define ORXPP_BANK_DECL_H

#include "memory/memory.h"
#include "base/type.h"

namespace orx
{

template<class... T> class Bank;

template<class T>
class Bank<T>
{
public:
    using ValueType = T;

    // Constructors, destructor and assignment
    Bank();
    explicit Bank(U16 nbElem, U32 flags = {}, Memory::Type memType = Memory::Type::Main);

    Bank(const Bank& source) = delete;
    Bank(Bank&& source);

    Bank& operator=(const Bank& source) = delete;
    Bank& operator=(Bank&& source);

    virtual ~Bank();

    // Validity and equality check
    operator bool() const;
    bool operator==(const Bank& other) const;
    bool operator!=(const Bank& other) const;

    // Basic handling
    static Bank Create(U16 nbElem, U32 flags = {},
                       Memory::Type memType = Memory::Type::Main);
    static Status Delete(Bank& bank);

    // Methods
    T *allocate();
    T *allocate(U32& index);

    void clear();
    void compact();
    void free(T *cell);

    T *at(U32 index) const;
    T *operator[](U32 index) const;

    U32 counter() const;

    U32 index(const T *cell) const;
    T *next(const T *cell) const;

protected:
    Bank(orxBANK *source);

private:
    orxBANK *bank;
    bool owns;
};

template<>
class Bank<>
{
public:
    // Internal module function
    static Status Init();
    static void Setup();
    static void Exit();

    static void CompactAll();
};

}

#endif // ORXPP_BANK_DECL_H
