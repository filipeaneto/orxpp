#ifndef ORXPP_EVENT_H
#define ORXPP_EVENT_H

#include <orx.h>

#include "base/type.h"

namespace orx {

class Event
{
public:
    using Handler = orxEVENT_HANDLER;

    enum class Type : U32
    {
        Anim, Clock, Config, Display, FX, Input, Locale, Object, Render, Physics,
        Resource, Shader, Sound, Spawner, System, Texture, Timeline, CoreNumber,
        FirstReserved   = CoreNumber,
        LastReserved    = orxEVENT_TYPE_LAST_RESERVED,
        UserDefined     = orxEVENT_TYPE_USER_DEFINED,
        None            = orxENUM_NONE
    };

    friend constexpr Type operator+(Type type, U32 value);

    static Status Init();
    static void Setup();
    static void Exit();

    static bool IsSending();

    template<class C, class E> static Handler AddHandler()
    {
        orxEVENT_TYPE eType = static_cast<orxEVENT_TYPE>(E::EventType);

        auto handler = [](const orxEVENT *event) -> orxSTATUS {
            orxASSERT(static_cast<orxEVENT_TYPE>(E::EventType) == event->eType);
            return C::EventHandler(E{event});
        };

        orxSTATUS result = orxEvent_AddHandler(eType, handler);
        return result == orxSTATUS_SUCCESS ? handler : nullptr;
    }

    template<class E> static Status RemoveHandler(Handler handler)
    {
        orxEVENT_TYPE eType = static_cast<orxEVENT_TYPE>(E::EventType);
        return orxEvent_RemoveHandler(eType, handler);
    }

    Status send() const;

    template<class E> static Status Send(typename E::ID id)
    {
        orxEVENT_TYPE eType = static_cast<orxEVENT_TYPE>(E::EventType);
        orxENUM eID = static_cast<orxENUM>(id);

        return orxEvent_SendShort(eType, eID);
    }

protected:
    Event(const orxEVENT *source);

    Event(const Event& source) = default;
    Event(Event&& source) = default;

    U32 id() const;
    void *sender() const;
    void *recipient() const;
    void *payload() const;

private:
    const orxEVENT *event;

    Event& operator=(const Event& source) = default;
    Event& operator=(Event&& source) = default;
};

} // namespace orx

#endif // ORXPP_EVENT_H
