#ifndef ORXPP_STRUCTURE_H
#define ORXPP_STRUCTURE_H

#include <orx.h>

namespace orx
{

class Anim;
class AnimPointer;
class AnimSet;

class Structure
{
public:
    enum class ID;

    friend class Anim;
    friend class AnimPointer;
    friend class AnimSet;

protected:
    Structure(orxSTRUCTURE *source);

private:
    orxSTRUCTURE *structure;
    bool owns;
};

Structure::Structure(orxSTRUCTURE *source) : structure{source}, owns{false} {}

}

#endif // ORXPP_STRUCTURE_H
