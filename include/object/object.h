#ifndef ORXPP_OBJECT_H
#define ORXPP_OBJECT_H

#include <orx.h>

#include "object/structure.h"
#include "display/display.h"
#include "utils/string.h"
#include "memory/bank.h"
#include "core/event.h"
#include "base/type.h"

#include <cstddef>

namespace orx
{

namespace Physics { class Event; }

class Texture;
class Camera;
class Vector;
class Sound;
class Frame;
class Clock;
class OBox;
class AnimSet;
class Spawner;
class Sound;
class Anim;

class Object
{
public:
    class Event : private orx::Event
    {
    public:
        static constexpr orx::Event::Type EventType{orx::Event::Type::Object};

        enum class ID : U32
        {
            Create = 0, Delete, Enable, Disable, Pause, Unpause,
            Number, None = orxENUM_NONE
        };

        Event(const orx::Event& source);

        inline ID id() const;
        inline Object sender() const;
    };

    // Constructors, destructor and assignment
    Object();
    explicit Object(std::nullptr_t);
    explicit Object(ConstString configID);
    Object(const Object& source) = delete;
    Object(Object&& source);

    Object& operator=(const Object& source) = delete;
    Object& operator=(Object&& source);

    virtual ~Object();

    // Validity and equality check
    operator bool() const;
    bool operator==(const Object& other) const;
    bool operator!=(const Object& other) const;

    // Internal module function
    inline static void   Setup();
    inline static Status Init();
    inline static void   Exit();

    // Basic handling
    inline static Object Create(std::nullptr_t);
    inline static Object Create(ConstString configID);

    inline static Status Delete(Object& object);

    inline void enable(bool e = true);
    inline bool enabled() const;
    inline void pause(bool e = true);
    inline bool paused() const;

    // User data
    template<class T> inline void userData(T& data);
    template<class T> inline T& userData() const;

    // Ownership
    template<class T> inline void owner(T& owner);
    template<class T> inline T& owner() const;
    inline Object ownedChild() const;
    inline Object ownedSibling() const;

    // Clock
    inline Status clock(Clock& clock);
    inline Clock clock() const;

    // Link structures
    inline Status linkStructure(Structure& structure);
    inline void unlinkStructure(Structure::ID structureID);
    inline Structure structure(Structure::ID structureID) const;

    // Flip
    inline Status flip(bool flipX, bool flipY);
    inline Bool2 flip() const;

    // Graphic
    inline Status pivot(const Vector& p);
    inline Vector pivot() const;
    inline Status origin(const Vector& o);
    inline Vector origin() const;
    inline Status size(const Vector& s);
    inline Vector size() const;

    inline Status color(std::nullptr_t);
    inline Status color(const Color& c);
    inline Vector color() const;
    inline bool hasColor() const;

    inline Status repeat(Float repeatX, Float repeatY);
    inline Float2 repeat() const;

    inline Status blendMode(Display::BlendMode mode);
    inline Display::BlendMode blendMode() const;

    // Frame
    inline Status position(const Vector& p);
    inline Vector position() const;
    inline Status worldPosition(const Vector& p);
    inline Vector worldPosition() const;
    inline Status rotation(Float rotation);
    inline Float rotation() const;
    inline Status worldRotation(Float rotation);
    inline Float worldRotation() const;
    inline Status scale(const Vector& p);
    inline Vector scale() const;
    inline Status worldScale(const Vector& p);
    inline Vector worldScale() const;

    // Parent
    inline Status parent(std::nullptr_t);
    inline Status parent(Object& p);
    inline Status parent(Spawner& p);
    inline Status parent(Camera& p);
    inline Status parent(Frame& p);
    inline Structure parent() const;

    inline Structure child() const;
    inline Structure sibling() const;

    inline Status attach(Object& p);
    inline Status attach(Spawner& p);
    inline Status attach(Camera& p);
    inline Status attach(Frame& p);
    inline Status detach();

    // Animation
    inline Status animSet(AnimSet& animSet);
    inline Status animFrequency(Float frequency);
    inline Status currentAnim(ConstString animName);
    inline bool isCurrentAnim(ConstString animName) const;
    inline Status targetAnim(ConstString animName);
    inline bool isTargetAnim(ConstString animName) const;

    // Physics and dynamics
    inline Status speed(const Vector& s);
    inline Vector speed() const;
    inline Status relativeSpeed(const Vector& s);
    inline Vector relativeSpeed() const;
    inline Status angularVelocity(Float v);
    inline Float angularVelocity() const;
    inline Status customGravity(const Vector& g);
    inline Vector customGravity() const;

    inline Float mass() const;
    inline Vector massCenter() const;

    inline Status applyTorque(Float torque);
    inline Status applyForce(const Vector& force, const Vector& point);
    inline Status applyForce(const Vector& force);
    inline Status applyImpulse(const Vector& impulse, const Vector& point);
    inline Status applyImpulse(const Vector& impulse);

    inline static Object Raycast(const Vector& start,
                                 const Vector& end,
                                 U16 selfFlags = 0xFFFF,
                                 U16 checkMask = 0xFFFF,
                                 bool earlyExit = true);
    inline static Object Raycast(const Vector& start,
                                 const Vector& end,
                                 U16 selfFlags,
                                 U16 checkMask,
                                 bool earlyExit,
                                 Vector& contact);
    inline static Object Raycast(const Vector& start,
                                 const Vector& end,
                                 U16 selfFlags,
                                 U16 checkMask,
                                 bool earlyExit,
                                 Vector& contact,
                                 Vector& normal);

    // Text
    inline Status textString(ConstString string);
    inline ConstString textString() const;

    // Bounding box
    inline OBox boundingBox() const;

    // FX
    inline Status addFX(ConstString configID);
    inline Status addUniqueFX(ConstString configID);
    inline Status addDelayedFX(ConstString configID, Float delay);
    inline Status addUniqueDelayedFX(ConstString configID, Float delay);
    inline Status removeFX(ConstString configID);
    inline Status synchronizeFX(const Object& model);

    // Sound
    inline Status addSound(ConstString configID);
    inline Status removeSound(ConstString configID);
    inline Sound lastAddedSound() const;
    inline Status volume(Float v);
    inline Status pitch(Float p);

    // Shader
    inline Status addShader(ConstString configID);
    inline Status removeShader(ConstString configID);
    inline void enableShader(bool e = true);
    inline bool shaderEnabled() const;

    // Timeline
    inline Status addTimeLineTrack(ConstString configID);
    inline Status removeTimeLineTrack(ConstString configID);
    inline void enableTimeLine(bool e = true);
    inline bool timeLineEnabled() const;

    // Name
    inline ConstString name() const;

    // Neighboring
    inline static Bank<Object> CreateNeighborList(const OBox& checkBox);

    // Smoothing
    inline Status smoothing(Display::Smoothing s);
    inline Display::Smoothing smoothing() const;

    // Texture
    inline Texture workingTexture() const;

    // Life time and active time
    inline Status lifeTime(Float l);
    inline Float lifeTime() const;
    inline Float activeTime() const;

    // Group
    inline static U32 GetDefaultGroupID();
    inline Status groupID(U32 id);
    inline U32 groupID() const;

    inline static Object GetFirst(U32 groupID = orxU32_UNDEFINED);
    inline static Object GetNext(U32 groupID = orxU32_UNDEFINED);

    inline Object next(U32 groupID);
    inline Object next();

    // Picking
    inline static Object Pick(const Vector& position, U32 groupID = orxU32_UNDEFINED);
    inline static Object BoxPick(const OBox& box, U32 groupID = orxU32_UNDEFINED);

    // Friendship
    friend class Physics::Event;
    friend class Object::Event;
    friend class Sound;
    friend class Anim;

protected:
    Object(orxOBJECT *source);

private:
    orxOBJECT *object;
    bool owns;
};

Object::Event::ID Object::Event::id() const
{
    return static_cast<ID>(orx::Event::id());
}

Object Object::Event::sender() const
{
    return orxOBJECT(orx::Event::sender());
}

Object::Event::Event(const orx::Event& source) : orx::Event{source} {}

Object::Object() : object{nullptr}, owns{true} {}

Object::Object(std::nullptr_t) :
    object{orxObject_Create()}, owns{true} {}

Object::Object(ConstString configID) :
    object{orxObject_CreateFromConfig(configID)}, owns{true} {}

Object::Object(Object&& source) :
    object{source.object}, owns{source.owns}
{
    source.object = nullptr;
}

Object::Object(orxOBJECT *source) : object{source}, owns{false} {}

Object& Object::operator=(Object&& source)
{
    if (owns && object) orxObject_Delete(object);

    object = source.object;
    owns = source.owns;

    source.object = nullptr;
    return *this;
}

Object::~Object()
{
    if (owns && object) orxObject_Delete(object);
}

Object Object::Create(std::nullptr_t)
{
    return orxObject_Create();
}

Object Object::Create(ConstString configID)
{
    return orxObject_CreateFromConfig(configID);
}

Status Object::Delete(Object& object)
{
    Status result = static_cast<Status>(orxObject_Delete(object.object));
    object.object = nullptr;

    return result;
}

template<class T>
void Object::userData(T& data)
{
    orxObject_SetUserData(object, &data);
}

Status Object::rotation(Float r)
{
    return static_cast<Status>(orxObject_SetRotation(object, r));
}

Float Object::rotation() const
{
    return orxObject_GetRotation(object);
}

Status Object::scale(const Vector& s)
{
    return static_cast<Status>(orxObject_SetScale(object, &s.vector));
}

Vector Object::scale() const
{
    orxVECTOR result;
    return orxObject_GetScale(object, &result);
}

Status Object::addFX(ConstString configID)
{
    return static_cast<Status>(orxObject_AddFX(object, configID));
}

Status Object::parent(Object& p)
{
    return static_cast<Status>(orxObject_SetParent(object, p.object));
}

Vector Object::worldPosition() const
{
    orxVECTOR result;
    return orxObject_GetWorldPosition(object, &result);
}

Status Object::position(const Vector& position)
{
    return static_cast<Status>(orxObject_SetPosition(object, &position.vector));
}

ConstString Object::name() const
{
    return orxObject_GetName(object);
}

Status Object::targetAnim(ConstString animName)
{
    return static_cast<Status>(orxObject_SetTargetAnim(object, animName));
}

Status Object::color(const Color& c)
{
    return static_cast<Status>(orxObject_SetColor(object, &c.color));
}

Status Object::addSound(ConstString configID)
{
    return static_cast<Status>(orxObject_AddSound(object, configID));
}

void Object::enable(bool e) { orxObject_Enable(object, e); }

}

#endif // ORXPP_OBJECT_H
