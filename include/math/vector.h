#ifndef ORXPP_VECTOR_H
#define ORXPP_VECTOR_H

#include <orx.h>

#include "base/type.h"

namespace orx
{

class AABox; class ConstString; class Viewport; class Vector; class Object;
class Camera; class Sound; class Color;

namespace Config {
    Vector GetVector(ConstString sectionName);
    Status Set(ConstString key, const Vector& value);
}

namespace Physics {
    Vector GetGravity();
    Status SetGravity(const Vector&);
}

namespace Render {
    bool GetWorldPosition(const Vector&, const Viewport&, Vector&);
    bool GetWorldPosition(const Vector&, Vector&);
    Vector GetWorldPosition(const Vector&, const Viewport&);
}

namespace Mouse {
    Vector GetPosition();
}

namespace SoundSystem {
    orx::Status SetListenerPosition(const Vector& position);
    Vector GetListenerPosition();
    class Sound;
}

class Vector
{
public:
    // Constructors
    constexpr Vector(Float value = {}) :
        vector{{value}, {value}, {value}} {}

    constexpr Vector(Float x, Float y, Float z = {}) :
        vector{{x}, {y}, {z}} {}

    // Member access
    Float x() const; Float& x();
    Float y() const; Float& y();
    Float z() const; Float& z();

    Float r() const; Float& r();
    Float g() const; Float& g();
    Float b() const; Float& b();

    Float h() const; Float& h();
    Float s() const; Float& s();
    Float v() const; Float& v();
    Float l() const; Float& l();

    Float rho() const; Float& rho();
    Float theta() const; Float& theta();
    Float phi() const; Float& phi();

    // Methods
    Float dot2D(const Vector& op2) const;
    static Float Dot2D(const Vector& op1, const Vector& op2);

    Vector rotate2D(Float angle) const;
    static Vector Rotate2D(const Vector& op, Float angle);

    Vector operator+(const Vector& op2) const;

    Vector operator-(const Vector& op2) const;
    Vector operator-() const;

    Vector operator*(const Vector& op2) const;
    Vector operator*(Float op2) const;
    friend Vector operator*(Float op1, const Vector& op2);

    Vector operator/(const Vector& op2) const;
    Vector operator/(Float op2) const;
    friend Vector operator/(Float op1, const Vector& op2);

    bool operator==(const Vector& op2) const;
    bool operator!=(const Vector& op2) const;

    operator bool() const;

    static Vector CatmullRom(const Vector& point1, const Vector& point2,
                             const Vector& point3, const Vector& point4, Float t);

    Vector clamp(const Vector& min, const Vector& max) const;
    static Vector Clamp(const Vector& op, const Vector& min, const Vector& max);

    Vector cross(const Vector& op2) const;
    static Vector Cross(const Vector& op1, const Vector& op2);

    Float dot(const Vector& op2) const;
    static Float Dot(const Vector& op1, const Vector& op2);

    Vector fromCartesianToSpherical() const;
    Vector fromSphericalToCartesian() const;

    Float distance(const Vector& op2) const;
    static Float Distance(const Vector& op1, const Vector& op2);

    Float squareDistance(const Vector& op2) const;
    static Float SquareDistance(const Vector& op1, const Vector& op2);

    Vector lerp(const Vector& op2, Float op) const;
    static Vector Lerp(const Vector& op1, const Vector& op2, Float op);

    static Vector Max(const Vector& op1, const Vector& op2);
    static Vector Min(const Vector& op1, const Vector& op2);

    Float size() const;
    Float squareSize() const;

    Vector floor() const;
    Vector normalize() const;
    Vector rec() const;
    Vector round() const;

    Vector& set(Float value);
    Vector& set(Float x, Float y, Float z = {});

    // Friendship
    friend Vector Physics::GetGravity();
    friend Status Physics::SetGravity(const Vector&);
    friend bool Render::GetWorldPosition(const Vector&, const Viewport&, Vector&);
    friend bool Render::GetWorldPosition(const Vector&, Vector&);
    friend Vector Render::GetWorldPosition(const Vector&, const Viewport&);
    friend Vector Mouse::GetPosition();
    friend orx::Status SoundSystem::SetListenerPosition(const Vector& position);
    friend Vector SoundSystem::GetListenerPosition();
    friend Vector Config::GetVector(ConstString sectionName);
    friend Status Config::Set(ConstString key, const Vector& value);

    friend class SoundSystem::Sound;
    friend class ConstString;
    friend class String;
    friend class Viewport;
    friend class Object;
    friend class Camera;
    friend class Sound;
    friend class AABox;
    friend class Color;

    static const Vector Zero;
    static const Vector One;
    static const Vector Black;
    static const Vector Blue;
    static const Vector Green;
    static const Vector Red;
    static const Vector White;
    static const Vector X;
    static const Vector Y;
    static const Vector Z;

protected:
    constexpr Vector(const orxVECTOR *source) :
        vector{{source->fX}, {source->fY}, {source->fZ}} {}

private:
    orxVECTOR vector;
};

}

#endif // ORXPP_VECTOR_H
