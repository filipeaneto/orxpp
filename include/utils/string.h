#ifndef ORXPP_STRING_H
#define ORXPP_STRING_H

#include <orx.h>

#include "base/type.h"

#include <iostream>
#include <cstddef>
#include <string>

namespace orx
{

class String;
class Vector;

using Char = orxCHAR;

class ConstString
{
public:
    // Constructors, destructor and assignment
    constexpr ConstString(const orxSTRING source = nullptr) : string{source} {}

    constexpr ConstString(const ConstString& source) = default;
    constexpr ConstString(ConstString&& source) = default;

    ConstString& operator=(const ConstString& source) = default;
    ConstString& operator=(ConstString&& source) = default;

    ~ConstString() = default;

    // Validity and equality checks
    operator bool() const;
    bool operator==(ConstString other) const;
    bool operator!=(ConstString other) const;
    bool operator<(ConstString other) const;

    // Cast
    operator const orxSTRING() const;

    // Methods
    U32 continueCRC(U32 crc) const;
    U32 continueCRC(U32 crc, U32 charNumber) const;

    U32 characterCounter() const;
    U32 length() const;

    ConstString extension() const;
    U32 firstCharacterCodePoint(ConstString& remaining) const;
    U32 firstCharacterCodePoint() const;

    S32 compare(ConstString other) const;
    S32 compare(ConstString other, U32 charNumber) const;
    S32 icompare(ConstString other) const;
    S32 icompare(ConstString other, U32 charNumber) const;

    ConstString searchChar(Char c) const;
    S32 searchChar(Char c, U32 position) const;
    ConstString searchString(ConstString string2) const;

    ConstString skipPath() const;
    ConstString skipWhiteSpaces() const;

    U32 toCRC() const;
    U32 toCRC(U32 charNumber) const;

    Status toBool(bool& outValue, ConstString& remaining) const;
    Status toBool(bool& outValue, std::nullptr_t) const;
    bool toBool() const;

    Status toFloat(Float& outValue, ConstString& remaining) const;
    Status toFloat(Float& outValue, std::nullptr_t) const;
    Float toFloat() const;

    Status toS32(S32& outValue, ConstString& remaining) const;
    Status toS32(S32& outValue, std::nullptr_t) const;
    S32 toS32() const;

    Status toS32(S32& outValue, U32 base, ConstString& remaining) const;
    Status toS32(S32& outValue, U32 base, std::nullptr_t) const;
    S32 toS32(U32 base) const;

    Status toS64(S64& outValue, ConstString& remaining) const;
    Status toS64(S64& outValue, std::nullptr_t) const;
    S64 toS64() const;

    Status toS64(S64& outValue, U32 base, ConstString& remaining) const;
    Status toS64(S64& outValue, U32 base, std::nullptr_t) const;
    S64 toS64(U32 base) const;

    Status toU32(U32& outValue, ConstString& remaining) const;
    Status toU32(U32& outValue, std::nullptr_t) const;
    U32 toU32() const;

    Status toU32(U32& outValue, U32 base, ConstString& remaining) const;
    Status toU32(U32& outValue, U32 base, std::nullptr_t) const;
    U32 toU32(U32 base) const;

    Status toU64(U64& outValue, ConstString& remaining) const;
    Status toU64(U64& outValue, std::nullptr_t) const;
    U64 toU64() const;

    Status toU64(U64& outValue, U32 base, ConstString& remaining) const;
    Status toU64(U64& outValue, U32 base, std::nullptr_t) const;
    U64 toU64(U32 base) const;

    Status toVector(Vector& outValue, ConstString& remaining) const;
    Status toVector(Vector& outValue, std::nullptr_t) const;
    Vector toVector() const;

    friend std::ostream& operator<<(std::ostream& os, const ConstString& s);
    friend class String;

private:
    const orxSTRING string;
};

class String
{
public:
    // Constructors, destructor and assignment
    String();

    String(const String& source) = default;
    String(ConstString source, U32 charNumber);
    String(ConstString source);
    String(String&& source) = default;

    String& operator=(const String& source) = default;
    String& operator=(ConstString source);
    String& operator=(String&& source) = default;

    virtual ~String() = default;

    // Validity and equality checks
    operator bool() const;
    bool operator==(ConstString other) const;
    bool operator!=(ConstString other) const;
    bool operator<(ConstString other) const;

    // Cast
    operator ConstString() const;

    // Internal module function
    static Status Init();
    static void Setup();
    static void Exit();

    // Static functions
    static ConstString GetFromID(U32 id);
    static U32 GetID(ConstString string);

    static U32 GetUTF8CharacterLength(U32 characterCodePoint);
    static bool IsCharacterAlphaNumeric(U32 characterCodePoint);
    static bool IsCharacterASCII(U32 characterCodePoint);

    // Methods
    String& copy(ConstString source);
    String& copy(ConstString source, U32 charNumber);

    template<class... Tail>
    S32 print(U32 charNumber, ConstString source, Tail... tail);
    template<class... Tail>
    S32 print(ConstString source, Tail... tail);
    U32 print(U32 size, U32 characterCodePoint);

    String& lowerCase();
    String& upperCase();

    U32 continueCRC(U32 crc) const;
    U32 continueCRC(U32 crc, U32 charNumber) const;

    U32 characterCounter() const;
    U32 length() const;

    ConstString extension() const;
    U32 firstCharacterCodePoint(ConstString& remaining) const;
    U32 firstCharacterCodePoint() const;

    S32 compare(ConstString other) const;
    S32 compare(ConstString other, U32 charNumber) const;
    S32 icompare(ConstString other) const;
    S32 icompare(ConstString other, U32 charNumber) const;

    ConstString searchChar(Char c) const;
    S32 searchChar(Char c, U32 position) const;
    ConstString searchString(ConstString string2) const;

    ConstString skipPath() const;
    ConstString skipWhiteSpaces() const;

    U32 toCRC() const;
    U32 toCRC(U32 charNumber) const;

    Status toBool(bool& outValue, ConstString& remaining) const;
    Status toBool(bool& outValue, std::nullptr_t) const;
    bool toBool() const;

    Status toFloat(Float& outValue, ConstString& remaining) const;
    Status toFloat(Float& outValue, std::nullptr_t) const;
    Float toFloat() const;

    Status toS32(S32& outValue, ConstString& remaining) const;
    Status toS32(S32& outValue, std::nullptr_t) const;
    S32 toS32() const;

    Status toS32(S32& outValue, U32 base, ConstString& remaining) const;
    Status toS32(S32& outValue, U32 base, std::nullptr_t) const;
    S32 toS32(U32 base) const;

    Status toS64(S64& outValue, ConstString& remaining) const;
    Status toS64(S64& outValue, std::nullptr_t) const;
    S64 toS64() const;

    Status toS64(S64& outValue, U32 base, ConstString& remaining) const;
    Status toS64(S64& outValue, U32 base, std::nullptr_t) const;
    S64 toS64(U32 base) const;

    Status toU32(U32& outValue, ConstString& remaining) const;
    Status toU32(U32& outValue, std::nullptr_t) const;
    U32 toU32() const;

    Status toU32(U32& outValue, U32 base, ConstString& remaining) const;
    Status toU32(U32& outValue, U32 base, std::nullptr_t) const;
    U32 toU32(U32 base) const;

    Status toU64(U64& outValue, ConstString& remaining) const;
    Status toU64(U64& outValue, std::nullptr_t) const;
    U64 toU64() const;

    Status toU64(U64& outValue, U32 base, ConstString& remaining) const;
    Status toU64(U64& outValue, U32 base, std::nullptr_t) const;
    U64 toU64(U32 base) const;

    Status toVector(Vector& outValue, ConstString& remaining) const;
    Status toVector(Vector& outValue, std::nullptr_t) const;
    Vector toVector() const;

    friend std::ostream& operator<<(std::ostream& os, const String& s);

private:
    std::string string;
};

template<class... Tail>
S32 String::print(U32 charNumber, ConstString source, Tail... tail)
{
    char *buffer = new char[charNumber + 1];
    S32 result = orxString_NPrint(buffer, charNumber, source, tail...);

    string += std::move(buffer);

    delete[] buffer;

    return result;
}

template<class... Tail>
S32 String::print(ConstString source, Tail... tail)
{
    char *buffer = new char[string.capacity() - string.length()];
    S32 result = orxString_Print(buffer, source, tail...);

    string += std::move(buffer);

    delete[] buffer;

    return result;
}

}

#endif // ORXPP_STRING_H
