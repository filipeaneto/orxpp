#ifndef ORXPP_DISPLAY_H
#define ORXPP_DISPLAY_H

#include <orx.h>

#include "math/vector.h"
#include "base/type.h"

namespace orx
{

class Object;

class Color
{
public:
    Color(const Vector& v = {}, Float f = orxFLOAT_1) :
        color{{v.vector}, f} {}

    friend class Object;

private:
    orxCOLOR color;
};

class Display
{
public:
    enum class BlendMode;
    enum class Smoothing;

};

}

#endif // ORXPP_DISPLAY_H
