#ifndef ORXPP_ANIMSET_H
#define ORXPP_ANIMSET_H

#include <orx.h>

#include "base/type.h"

namespace orx
{

class ConstString;
class AnimPointer;
class Structure;
class Anim;

class AnimSet
{
public:
    class LinkTable
    {
    public:
        LinkTable();

        LinkTable(const LinkTable& source) = delete;
        LinkTable(LinkTable&& source) = default;

        LinkTable& operator=(const LinkTable& source) = delete;
        LinkTable& operator=(LinkTable&& source) = default;

        ~LinkTable();

        friend class AnimSet;

    protected:
        LinkTable(orxANIMSET_LINK_TABLE *source);

    private:
        orxANIMSET_LINK_TABLE *linkTable;
    };

    // Constructors, destructor and assignment
    AnimSet();
    explicit AnimSet(U32 size);
    explicit AnimSet(ConstString configID);
    AnimSet(const AnimSet& source) = delete;
    AnimSet(AnimSet&& source);

    AnimSet& operator=(const AnimSet& source) = delete;
    AnimSet& operator=(AnimSet&& source);

    virtual ~AnimSet();

    // Structure operations
    AnimSet(const Structure& source);
    AnimSet& operator=(const Structure& source);

    operator Structure() const;

    // Validity and equality check
    operator bool() const;
    bool operator==(const AnimSet& other) const;
    bool operator!=(const AnimSet& other) const;

    // Internal module function
    static Status Init();
    static void Setup();
    static void Exit();

    // Basic handling
    static AnimSet Create(U32 size);
    static AnimSet Create(ConstString configID);

    static Status Delete(AnimSet& animSet);

    // Methods
    U32 addAnim(Anim& anim);
    Status removeAnim(U32 animID);
    Status removeAllAnims();

    U32 addLink(U32 srcAnim, U32 dstAnim);
    Status removeLink(U32 linkID);

    void addReference();
    void removeReference();

    LinkTable cloneLinkTable() const;

    U32 computeAnim(U32 srcAnim, U32 dstAnim, // in
                           Float& time, LinkTable& linkTable, // in, out
                           bool& cut, bool& clearTarget); // out

    U32 findNextAnim(U32 srcAnim, U32 dstAnim = orxU32_UNDEFINED);

    Anim anim(U32 animID) const;
    U32 animCounter() const;
    U32 animID(ConstString animName) const;

    U32 animStorageSize() const;

    U32 link(U32 srcAnim, U32 dstAnim) const;
    Status linkProperty(U32 linkID, U32 property, U32 value) ;
    U32 linkProperty(U32 linkID, U32 property) const;

    friend class AnimPointer;

protected:
    AnimSet(orxANIMSET *source);

private:
    orxANIMSET *animSet;
    bool owns;
};

}

#endif // ORXPP_ANIMSET_H
