#ifndef ORXPP_ANIMPOINTER_H
#define ORXPP_ANIMPOINTER_H

#include <orx.h>

#include "base/type.h"

namespace orx
{

class ConstString;
class Structure;
class AnimSet;

class AnimPointer
{
public:
    // Constructors, destructor and assignment
    AnimPointer();
    explicit AnimPointer(const Structure& owner, AnimSet& animSet);
    explicit AnimPointer(const Structure& owner, ConstString configID);
    AnimPointer(const AnimPointer& source) = delete;
    AnimPointer(AnimPointer&& source);

    AnimPointer& operator=(const AnimPointer& source) = delete;
    AnimPointer& operator=(AnimPointer&& source);

    virtual ~AnimPointer();

    // Structure operations
    AnimPointer(const Structure& source);
    AnimPointer& operator=(const Structure& source);

    operator Structure() const;

    // Validity and equality check
    operator bool() const;
    bool operator==(const AnimPointer& other) const;
    bool operator!=(const AnimPointer& other) const;

    // Internal module function
    static Status Init();
    static void Setup();
    static void Exit();

    // Basic handling
    static AnimPointer Create(const Structure& owner, AnimSet& animSet);
    static AnimPointer Create(const Structure& owner, ConstString configID);

    static Status Delete(AnimPointer& animPointer);

    // Methods
    AnimSet animSet() const;

    Status currentAnim(U32 animID);
    Status currentAnim(ConstString animName);
    U32 currentAnim() const;
    Structure currentAnimData() const;
    ConstString currentAnimName() const;

    Status time(Float t);
    Float currentTime() const;

    Status frequency(Float f);
    Float frequency() const;

    Structure owner() const;

    Status targetAnim(U32 animID);
    Status targetAnim(ConstString animName);
    U32 targetAnim() const;
    ConstString targetAnimName() const;

    Status pause(bool p = true);

protected:
    AnimPointer(orxANIMPOINTER *source);

private:
    orxANIMPOINTER *animPointer;
    bool owns;
};

}

#endif // ORXPP_ANIMPOINTER_H
