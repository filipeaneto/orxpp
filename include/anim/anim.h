#ifndef ORXPP_ANIM_H
#define ORXPP_ANIM_H

#include <orx.h>

#include "core/event.h"
#include "base/type.h"

namespace orx
{

class ConstString;
class Structure;
class AnimSet;
class Object;

class Anim
{
public:
    class CustomEvent
    {
    public:
        // Validity and equality check
        operator bool() const;
        bool operator==(const CustomEvent& other) const;
        bool operator!=(const CustomEvent& other) const;

        // Members access
        Float timeStamp() const;
        Float value() const;
        ConstString name() const;

        friend class Anim;

    protected:
        CustomEvent(const orxANIM_CUSTOM_EVENT* source);

    private:
        const orxANIM_CUSTOM_EVENT *customEvent;
    };

    class Event : private orx::Event
    {
    public:
        static constexpr orx::Event::Type EventType{orx::Event::Type::Anim};

        enum class ID : U32
        {
            Start = 0, Stop, Cut, Loop, CustomEvent, Number, None = orxENUM_NONE
        };

        Event(const orx::Event& source);

        ID id() const;

        Float customEventTime() const;
        Float customEventValue() const;

        Anim anim() const;

        ConstString animName() const;
        ConstString customEventName() const;

        Object recipient() const;
    };

    // Constructors, destructor and assignment
    Anim();
    explicit Anim(U32 flags, U32 keyNumber, U32 eventNumber);
    explicit Anim(ConstString configID);
    Anim(const Anim& source) = delete;
    Anim(Anim&& source);

    Anim& operator=(const Anim& source) = delete;
    Anim& operator=(Anim&& source);

    virtual ~Anim();

    // Structure operations
    Anim(const Structure& source);
    Anim& operator=(const Structure& source);

    operator Structure() const;

    // Validity and equality check
    operator bool() const;
    bool operator==(const Anim& other) const;
    bool operator!=(const Anim& other) const;

    // Internal module function
    static Status Init();
    static void Setup();
    static void Exit();

    // Basic handling
    static Anim Create(U32 flags, U32 keyNumber, U32 eventNumber);
    static Anim Create(ConstString configID);

    static Status Delete(Anim& anim);

    // Methods
    Status addEvent(ConstString eventName, Float timeStamp, Float value);
    Status addKey(Structure& data, Float timeStamp);

    U32 eventCounter() const;
    U32 eventStorageSize() const;
    U32 keyCounter() const;
    Structure keyData(U32 index);
    U32 keyStorageSize() const;

    Float length() const;
    ConstString name() const;
    CustomEvent nextEvent(Float timeStamp) const;

    void removeAllEvents();
    void removeAllKeys();
    Status removeLastEvent();
    Status removeLastKey();

    Status update(Float timeStamp);
    Status update(Float timeStamp, U32& currentKey);

    friend class AnimSet;
    friend class Anim::Event;

protected:
    Anim(orxANIM *source);

private:
    orxANIM *anim;
    bool owns;
};

}

#endif // ORXPP_ANIM_H
