#ifndef ORXPP_MOUSE_H
#define ORXPP_MOUSE_H

#include "orx.h"

namespace orx {
namespace Mouse {

Vector GetPosition()
{
    orxVECTOR result;
    return orxMouse_GetPosition(&result);
}

} // namespace Mouse
} // namespace orx

#endif // ORXPP_MOUSE_H
