#ifndef ORXPP_LOCALE_H
#define ORXPP_LOCALE_H

#include "orx.h"

#include "orxpp/string.h"
#include "orxpp/event.h"
#include "orxpp/type.h"

namespace orx
{

namespace Locale
{
    class Event : private orx::Event
    {
    public:
        static constexpr orx::Event::Type EventType{orx::Event::Type::Locale};

        enum class ID : U32
        {
            SelectLanguage = 0, SetString, Number, None = orxENUM_NONE
        };

        Event(const orx::Event& source);

        ID id() const;
        ConstString language() const;
        ConstString stringKey() const;
        ConstString stringValue() const;
    };

    inline Status Init();
    inline void Setup();
    inline void Exit();

    inline ConstString GetCurrentLanguage();
    inline ConstString GetKey(U32 keyIndex);
    inline U32 GetKeyCounter();
    inline ConstString GetLanguage(U32 languageIndex);
    inline U32 GetLanguageCounter();
    inline ConstString GetString(ConstString key);
    inline bool HasLanguage(ConstString language);
    inline bool HasString(ConstString key);
    inline Status SelectLanguage(ConstString language);
    inline Status SetString(ConstString key, ConstString value);
}

// Event
Locale::Event::Event(const orx::Event& source) : orx::Event{source} {}

Locale::Event::ID Locale::Event::id() const
{
    return static_cast<ID>(orx::Event::id());
}

ConstString Locale::Event::language() const
{
    orxASSERT(id() == ID::SelectLanguage);
    return static_cast<orxLOCALE_EVENT_PAYLOAD*>(payload())->zLanguage;
}

ConstString Locale::Event::stringKey() const
{
    orxASSERT(id() == ID::SetString);
    return static_cast<orxLOCALE_EVENT_PAYLOAD*>(payload())->zStringKey;
}

ConstString Locale::Event::stringValue() const
{
    orxASSERT(id() == ID::SetString);
    return static_cast<orxLOCALE_EVENT_PAYLOAD*>(payload())->zStringValue;
}

// Functions
void Locale::Exit()
{
    orxLocale_Exit();
}

ConstString Locale::GetCurrentLanguage()
{
    return orxLocale_GetCurrentLanguage();
}

ConstString Locale::GetKey(U32 keyIndex)
{
    return orxLocale_GetKey(keyIndex);
}

U32 Locale::GetKeyCounter()
{
    return orxLocale_GetKeyCounter();
}

ConstString Locale::GetLanguage(U32 languageIndex)
{
    return orxLocale_GetLanguage(languageIndex);
}

U32 Locale::GetLanguageCounter()
{
    return orxLocale_GetLanguageCounter();
}

ConstString Locale::GetString(ConstString key)
{
    return orxLocale_GetString(key);
}

bool Locale::HasLanguage(ConstString language)
{
    return orxLocale_HasLanguage(language);
}

bool Locale::HasString(ConstString key)
{
    return orxLocale_HasString(key);
}

Status Locale::Init()
{
    return static_cast<Status>(orxLocale_Init());
}

Status Locale::SelectLanguage(ConstString language)
{
    return static_cast<Status>(orxLocale_SelectLanguage(language));
}

Status Locale::SetString(ConstString key, ConstString value)
{
    return static_cast<Status>(orxLocale_SetString(key, value));
}

void Locale::Setup()
{
    orxLocale_Setup();
}

}

#endif // ORXPP_LOCALE_H
