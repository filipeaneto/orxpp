#ifndef ORXPP_AABOX_H
#define ORXPP_AABOX_H

#include "orx.h"

#include "orxpp/vector.h"

namespace orx
{

class AABox
{
public:
    AABox(const Vector& tl = {}, const Vector& br = {});
    AABox(const AABox& source) = default;
    AABox(AABox&& source) = default;

    AABox& operator=(const AABox& source) = default;
    AABox& operator=(AABox&& source) = default;

    Vector& bottomRight();
    Vector& topLeft();
    const Vector& bottomRight() const;
    const Vector& topLeft() const;

    Vector center() const;
    bool isInside(const Vector& position) const;

    AABox move(const Vector& m) const;

    AABox& reorder();
    AABox& set(const Vector& tl, const Vector& br);

    bool test2DIntersection(const AABox& other) const;
    bool testIntersection(const AABox& other) const;

protected:
    AABox(const orxAABOX *source);

private:
    Vector tl, br;
};

AABox::AABox(const Vector& tl, const Vector& br) : tl{tl}, br{br} {}

AABox::AABox(const orxAABOX *source) : tl{&source->vTL}, br{&source->vBR} {}

Vector& AABox::bottomRight() { return br; }
const Vector& AABox::bottomRight() const { return br; }

Vector& AABox::topLeft() { return tl; }
const Vector& AABox::topLeft() const { return tl; }

Vector AABox::center() const
{
    orxVECTOR result;
    orxAABOX aabox{.vTL = tl.vector, .vBR = br.vector};
    return orxAABox_GetCenter(&aabox, &result);
}

bool AABox::isInside(const Vector& position) const
{
    orxAABOX aabox{.vTL = tl.vector, .vBR = br.vector};
    return orxAABox_IsInside(&aabox, &position.vector);
}

AABox AABox::move(const Vector& m) const
{
    orxAABOX aabox{.vTL = tl.vector, .vBR = br.vector}, result;
    return orxAABox_Move(&result, &aabox, &m.vector);
}

AABox& AABox::reorder()
{
    orxAABOX aabox{.vTL = tl.vector, .vBR = br.vector};
    orxAABox_Reorder(&aabox);

    tl = &aabox.vTL;
    br = &aabox.vBR;

    return *this;
}

AABox& AABox::set(const Vector& tl, const Vector& br)
{
    this->tl = tl;
    this->br = br;
    return *this;
}

bool AABox::test2DIntersection(const AABox& other) const
{
    orxAABOX aabox1{.vTL = tl.vector, .vBR = br.vector};
    orxAABOX aabox2{.vTL = other.tl.vector, .vBR = other.br.vector};
    return orxAABox_Test2DIntersection(&aabox1, &aabox2);
}

bool AABox::testIntersection(const AABox& other) const
{
    orxAABOX aabox1{.vTL = tl.vector, .vBR = br.vector};
    orxAABOX aabox2{.vTL = other.tl.vector, .vBR = other.br.vector};
    return orxAABox_TestIntersection(&aabox1, &aabox2);
}

}

#endif // ORXPP_AABOX_H
