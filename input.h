#ifndef ORXPP_INPUT_H
#define ORXPP_INPUT_H

#include "orx.h"

#include "orxpp/string.h"
#include "orxpp/type.h"

namespace orx {

// TODO
using Enum = orxENUM;

namespace Input {

enum class Type : U32
{
    KeyboardKey = orxINPUT_TYPE_KEYBOARD_KEY,
    MouseButton,
    MouseAxis,
    JoystickButton,
    JoystickAxis,
    External,
    Number,
    None = orxENUM_NONE
};

inline Status GetBinding(ConstString name, U32 bindingIndex, Type& type, Enum& id)
{
    orxINPUT_TYPE _type;
    orxENUM _id;

    orxSTATUS result = orxInput_GetBinding(name, bindingIndex, &_type, &_id);

    type = static_cast<Type>(_type);
    id = static_cast<Enum>(_id);

    return static_cast<Status>(result);
}

inline ConstString GetBindingName(Type type, Enum id)
{
    return orxInput_GetBindingName(static_cast<orxINPUT_TYPE>(type),
                                   static_cast<orxENUM>(id));
}

inline bool HasNewStatus(ConstString inputName)
{
    return orxInput_HasNewStatus(inputName);
}

inline bool IsActive(ConstString inputName)
{
    return orxInput_IsActive(inputName);
}


} // namespace Input
} // namespace orx

#endif // ORXPP_INPUT_H
