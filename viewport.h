#ifndef ORXPP_VIEWPORT_H
#define ORXPP_VIEWPORT_H

#include "orx.h"

#include "orxpp/shaderpointer.h"
#include "orxpp/texture.h"
#include "orxpp/display.h"
#include "orxpp/string.h"
#include "orxpp/camera.h"
#include "orxpp/aabox.h"
#include "orxpp/type.h"

#include <cstddef>
#include <vector>

namespace orx
{

class Vector;
class Viewport;
namespace Render
{
    bool GetWorldPosition(const Vector&, const Viewport&, Vector&);
    Vector GetWorldPosition(const Vector&, const Viewport&);
}

class Viewport
{
public:
    // Constructors, destructor and assignment
    Viewport();
    explicit Viewport(std::nullptr_t);
    explicit Viewport(ConstString configID);
    Viewport(const Viewport& source) = delete;
    Viewport(Viewport&& source);

    Viewport& operator=(const Viewport& source) = delete;
    Viewport& operator=(Viewport&& source);

    virtual ~Viewport();

    // Validity and equality check
    operator bool() const;
    bool operator==(const Viewport& other) const;
    bool operator!=(const Viewport& other) const;

    // Methods
    inline static Status Init();
    inline static void Setup();
    inline static void Exit();

    inline static Viewport Create(std::nullptr_t);
    inline static Viewport Create(ConstString configID);

    inline static Status Delete(Viewport& viewport);

    inline Status addShader(ConstString configID);
    inline Status removeShader(ConstString configID);

    inline Status backgroundColor(std::nullptr_t);
    inline Status backgroundColor(const Color& color);
    inline Color backgroundColor() const;
    inline bool hasBackgroundColor() const;

    inline Status blendMode(Display::BlendMode mode);
    inline Display::BlendMode blendMode() const;

    inline AABox box() const;

    inline void camera(Camera& c);
    inline Camera camera() const;

    inline Float corretionRatio() const;

    inline void position(Float x, Float y);
    inline Float2 position() const;
    inline Status relativeSize(Float width, Float height);
    inline Float2 relativeSize() const;
    inline void size(Float width, Float height);
    inline Float2 size() const;

    inline Status relativePosition(U32 alignFlags);

    inline ShaderPointer shaderPointer() const;

    inline ConstString name() const;

    inline U32 textureCounter() const;
    inline void textureList(const std::vector<Texture>& list);
    inline std::vector<Texture> textureList(U32 number = orxU32_UNDEFINED) const; //!beware

    inline void enable(bool = true);
    inline bool enabled() const;
    inline void enableShader(bool = true);
    inline bool shaderEnabled() const;

    inline void alignment(U32 alignFlags);

    // Friendship
    friend bool Render::GetWorldPosition(const Vector&, const Viewport&, Vector&);
    friend Vector Render::GetWorldPosition(const Vector&, const Viewport&);

protected:
    Viewport(orxVIEWPORT *source);

private:
    orxVIEWPORT *viewport;
    bool owns;
};

Viewport::Viewport() : viewport{nullptr}, owns{true} {}

Viewport::Viewport(ConstString configID) :
    viewport{orxViewport_CreateFromConfig(configID)}, owns{true} {}

Viewport::Viewport(Viewport&& source) : viewport{source.viewport}, owns{source.owns}
{
    source.viewport = nullptr;
}

Viewport::Viewport(orxVIEWPORT *source) : viewport{source}, owns{false} {}

Viewport& Viewport::operator=(Viewport&& source)
{
    if (owns && viewport) orxViewport_Delete(viewport);

    viewport = source.viewport;
    owns = source.owns;

    source.viewport = nullptr;
    return *this;
}

Viewport::~Viewport()
{
    if (owns && viewport) orxViewport_Delete(viewport);
}

Viewport Viewport::Create(ConstString configID)
{
    return orxViewport_CreateFromConfig(configID);
}

Status Viewport::Delete(Viewport& viewport)
{
    Status result = static_cast<Status>(orxViewport_Delete(viewport.viewport));
    viewport.viewport = nullptr;

    return result;
}

Camera Viewport::camera() const
{
    return Camera{orxViewport_GetCamera(viewport)};
}

void Viewport::position(Float x, Float y)
{
    orxViewport_SetPosition(viewport, x, y);
}

Float2 Viewport::position() const
{
    Float x, y;
    orxViewport_GetPosition(viewport, &x, &y);
    return {x, y};
}

Status Viewport::relativeSize(Float width, Float height)
{
    return static_cast<Status>(orxViewport_SetRelativeSize(viewport, width, height));
}

Float2 Viewport::relativeSize() const
{
    Float w, h;
    orxViewport_GetRelativeSize(viewport, &w, &h);
    return {w, h};
}

}
#endif // ORXPP_VIEWPORT_H
