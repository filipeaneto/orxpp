#include "math/vector.h"

namespace orx
{

Float  Vector::x() const { return vector.fX; }
Float& Vector::x() { return vector.fX; }
Float  Vector::y() const { return vector.fY; }
Float& Vector::y() { return vector.fY; }
Float  Vector::z() const { return vector.fZ; }
Float& Vector::z() { return vector.fZ; }

Float  Vector::r() const { return vector.fR; }
Float& Vector::r() { return vector.fR; }
Float  Vector::g() const { return vector.fG; }
Float& Vector::g() { return vector.fG; }
Float  Vector::b() const { return vector.fB; }
Float& Vector::b() { return vector.fB; }

Float  Vector::h() const { return vector.fH; }
Float& Vector::h() { return vector.fH; }
Float  Vector::s() const { return vector.fS; }
Float& Vector::s() { return vector.fS; }
Float  Vector::v() const { return vector.fV; }
Float& Vector::v() { return vector.fV; }
Float  Vector::l() const { return vector.fL; }
Float& Vector::l() { return vector.fL; }

Float  Vector::rho() const { return vector.fRho; }
Float& Vector::rho() { return vector.fRho; }
Float  Vector::theta() const { return vector.fTheta; }
Float& Vector::theta() { return vector.fTheta; }
Float  Vector::phi() const { return vector.fPhi; }
Float& Vector::phi() { return vector.fPhi; }

Float Vector::dot2D(const Vector& op2) const
{
    return orxVector_2DDot(&vector, &op2.vector);
}

Float Vector::Dot2D(const Vector& op1, const Vector& op2)
{
    return op1.dot2D(op2);
}

Vector Vector::rotate2D(Float angle) const
{
    orxVECTOR result;
    return orxVector_2DRotate(&result, &vector, angle);
}

Vector Vector::Rotate2D(const Vector& op, Float angle)
{
    return op.rotate2D(angle);
}

Vector Vector::operator+(const Vector& op2) const
{
    orxVECTOR result;
    return orxVector_Add(&result, &vector, &op2.vector);
}


Vector Vector::operator-(const Vector& op2) const
{
    orxVECTOR result;
    return orxVector_Sub(&result, &vector, &op2.vector);
}

Vector Vector::operator-() const
{
    orxVECTOR result;
    return orxVector_Neg(&result, &vector);
}


Vector Vector::operator*(const Vector& op2) const
{
    orxVECTOR result;
    return orxVector_Mul(&result, &vector, &op2.vector);
}

Vector Vector::operator*(Float op2) const
{
    orxVECTOR result;
    return orxVector_Mulf(&result, &vector, op2);
}

Vector operator*(Float op1, const Vector& op2)
{
    return op2 * op1;
}

Vector Vector::operator/(const Vector& op2) const
{
    orxVECTOR result;
    return orxVector_Div(&result, &vector, &op2.vector);
}

Vector Vector::operator/(Float op2) const
{
    orxVECTOR result;
    return orxVector_Divf(&result, &vector, op2);
}

Vector operator/(Float op1, const Vector& op2)
{
    return op2 * op1;
}

bool Vector::operator==(const Vector& op2) const
{
    return orxVector_AreEqual(&vector, &op2.vector);
}

bool Vector::operator!=(const Vector& op2) const
{
    return !(*this == op2);
}

Vector::operator bool() const
{
    return !orxVector_IsNull(&vector);
}

Vector Vector::CatmullRom(const Vector& point1, const Vector& point2,
                          const Vector& point3, const Vector& point4, Float t)
{
    orxVECTOR result;
    return orxVector_CatmullRom(&result, &point1.vector, &point2.vector,
                                &point3.vector, &point4.vector, t);
}


Vector Vector::clamp(const Vector& min, const Vector& max) const
{
    orxVECTOR result;
    return orxVector_Clamp(&result, &vector, &min.vector, &max.vector);
}

Vector Vector::Clamp(const Vector& op, const Vector& min, const Vector& max)
{
    return op.clamp(min, max);
}


Vector Vector::cross(const Vector& op2) const
{
    orxVECTOR result;
    return orxVector_Cross(&result, &vector, &op2.vector);
}

Vector Vector::Cross(const Vector& op1, const Vector& op2)
{
    return op1.cross(op2);
}


Float Vector::dot(const Vector& op2) const
{
    return orxVector_Dot(&vector, &op2.vector);
}

Float Vector::Dot(const Vector& op1, const Vector& op2)
{
    return op1.dot(op2);
}


Float Vector::distance(const Vector& op2) const
{
    return orxVector_GetDistance(&vector, &op2.vector);
}

Float Vector::Distance(const Vector& op1, const Vector& op2)
{
    return op1.distance(op2);
}


Float Vector::squareDistance(const Vector& op2) const
{
    return orxVector_GetSquareDistance(&vector, &op2.vector);
}

Float Vector::SquareDistance(const Vector& op1, const Vector& op2)
{
    return op1.squareDistance(op2);
}

Vector Vector::lerp(const Vector& op2, Float op) const
{
    orxVECTOR result;
    return orxVector_Lerp(&result, &vector, &op2.vector, op);
}

Vector Vector::Lerp(const Vector& op1, const Vector& op2, Float op)
{
    return op1.lerp(op2, op);
}


Vector Vector::fromCartesianToSpherical() const
{
    orxVECTOR result;
    return orxVector_FromCartesianToSpherical(&result, &vector);
}

Vector Vector::fromSphericalToCartesian() const
{
    orxVECTOR result;
    return orxVector_FromSphericalToCartesian(&result, &vector);
}

Float Vector::size() const { return orxVector_GetSize(&vector); }
Float Vector::squareSize() const { return orxVector_GetSquareSize(&vector); }

Vector Vector::Max(const Vector& op1, const Vector& op2)
{
    orxVECTOR result;
    return orxVector_Max(&result, &op1.vector, &op2.vector);
}

Vector Vector::Min(const Vector& op1, const Vector& op2)
{
    orxVECTOR result;
    return orxVector_Min(&result, &op1.vector, &op2.vector);
}

Vector Vector::floor() const
{
    orxVECTOR result;
    return orxVector_Floor(&result, &vector);
}

Vector Vector::normalize() const
{
    orxVECTOR result;
    return orxVector_Normalize(&result, &vector);
}

Vector Vector::rec() const
{
    orxVECTOR result;
    return orxVector_Rec(&result, &vector);
}

Vector Vector::round() const
{
    orxVECTOR result;
    return orxVector_Round(&result, &vector);
}

Vector& Vector::set(Float value)
{
    orxVector_SetAll(&vector, value);
    return *this;
}

Vector& Vector::set(Float x, Float y, Float z)
{
    orxVector_Set(&vector, x, y, z);
    return *this;
}

// Variables
const Vector Vector::Zero{&orxVECTOR_0};
const Vector Vector::One{&orxVECTOR_1};
const Vector Vector::Black{&orxVECTOR_BLACK};
const Vector Vector::Blue{&orxVECTOR_BLUE};
const Vector Vector::Green{&orxVECTOR_GREEN};
const Vector Vector::Red{&orxVECTOR_RED};
const Vector Vector::White{&orxVECTOR_WHITE};
const Vector Vector::X{&orxVECTOR_X};
const Vector Vector::Y{&orxVECTOR_Y};
const Vector Vector::Z{&orxVECTOR_Z};

}
