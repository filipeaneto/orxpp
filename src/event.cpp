#include "core/event.h"

namespace orx
{

constexpr Event::Type operator+(Event::Type type, U32 value)
{
    return type == Event::Type::UserDefined ?
        static_cast<Event::Type>(static_cast<U32>(type) + value) :
        Event::Type::None;
}

Status Event::Init() { return orxEvent_Init(); }
void Event::Setup() { orxEvent_Setup(); }
void Event::Exit() { orxEvent_Exit(); }

bool Event::IsSending()
{
    return orxEvent_IsSending();
}

Status Event::send() const
{
    return orxEvent_Send(event);
}

Event::Event(const orxEVENT *source) : event{source} {}

U32 Event::id() const { return event->eID; }
void *Event::sender() const { return event->hSender; }
void *Event::recipient() const { return event->hRecipient; }
void *Event::payload() const { return event->pstPayload; }

}
