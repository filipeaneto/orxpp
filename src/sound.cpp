#include "sound/sound.h"

#include "sound/soundsystem.h"
#include "object/object.h"
#include "utils/string.h"
#include "math/vector.h"

namespace orx
{

// Event
Sound::Event::Event(const orx::Event& source) : orx::Event{source} {}

Sound::Event::ID Sound::Event::id() const
{
    return static_cast<ID>(orx::Event::id());
}

Object Sound::Event::recipient() const
{
    return orxOBJECT(orx::Event::recipient());
}

Sound Sound::Event::sound() const
{
    orxASSERT(id() != ID::Packet && id() != ID::RecordingPacket);
    auto payload = static_cast<orxSOUND_EVENT_PAYLOAD*>(orx::Event::payload());
    return payload->pstSound;
}

ConstString Sound::Event::soundName() const
{
    auto payload = static_cast<orxSOUND_EVENT_PAYLOAD*>(orx::Event::payload());
    return payload->zSoundName;
}

U32 Sound::Event::channelNumber() const
{
    orxASSERT(id() == ID::Packet || id() == ID::RecordingPacket);
    auto payload = static_cast<orxSOUND_EVENT_PAYLOAD*>(orx::Event::payload());
    return payload->stStream.stInfo.u32ChannelNumber;
}

U32 Sound::Event::sampleRate() const
{
    orxASSERT(id() == ID::Packet || id() == ID::RecordingPacket);
    auto payload = static_cast<orxSOUND_EVENT_PAYLOAD*>(orx::Event::payload());
    return payload->stStream.stInfo.u32SampleRate;
}

S16 *Sound::Event::sampleList() const
{
    orxASSERT(id() == ID::Packet || id() == ID::RecordingPacket);
    auto payload = static_cast<orxSOUND_EVENT_PAYLOAD*>(orx::Event::payload());
    return payload->stStream.stPacket.as16SampleList;
}

void Sound::Event::sampleList(S16 *sl) const
{
    orxASSERT(id() == ID::Packet || id() == ID::RecordingPacket);
    auto payload = static_cast<orxSOUND_EVENT_PAYLOAD*>(orx::Event::payload());
    payload->stStream.stPacket.as16SampleList = sl;
}

bool Sound::Event::discard() const
{
    orxASSERT(id() == ID::Packet || id() == ID::RecordingPacket);
    auto payload = static_cast<orxSOUND_EVENT_PAYLOAD*>(orx::Event::payload());
    return payload->stStream.stPacket.bDiscard;
}

void Sound::Event::discard(bool d) const
{
    orxASSERT(id() == ID::Packet || id() == ID::RecordingPacket);
    auto payload = static_cast<orxSOUND_EVENT_PAYLOAD*>(orx::Event::payload());
    payload->stStream.stPacket.bDiscard = d;
}

Float Sound::Event::timeStamp() const
{
    orxASSERT(id() == ID::Packet || id() == ID::RecordingPacket);
    auto payload = static_cast<orxSOUND_EVENT_PAYLOAD*>(orx::Event::payload());
    return payload->stStream.stPacket.fTimeStamp;
}

S32 Sound::Event::packetID() const
{
    orxASSERT(id() == ID::Packet || id() == ID::RecordingPacket);
    auto payload = static_cast<orxSOUND_EVENT_PAYLOAD*>(orx::Event::payload());
    return payload->stStream.stPacket.s32ID;
}

U32 Sound::Event::sampleNumber() const
{
    orxASSERT(id() == ID::Packet || id() == ID::RecordingPacket);
    auto payload = static_cast<orxSOUND_EVENT_PAYLOAD*>(orx::Event::payload());
    return payload->stStream.stPacket.u32SampleNumber;
}

void Sound::Event::sampleNumber(U32 sn) const
{
    orxASSERT(id() == ID::Packet || id() == ID::RecordingPacket);
    auto payload = static_cast<orxSOUND_EVENT_PAYLOAD*>(orx::Event::payload());
    payload->stStream.stPacket.u32SampleNumber = sn;
}

// Sound
Sound::Sound() : sound{nullptr}, owns{true} {}

Sound::Sound(std::nullptr_t) : sound{orxSound_Create()}, owns{true} {}

Sound::Sound(ConstString configID) :
    sound{orxSound_CreateFromConfig(configID)}, owns{true} {}

Sound::Sound(U32 channelNumber, U32 sampleRate, ConstString name) :
    sound{orxSound_CreateWithEmptyStream(channelNumber, sampleRate, name)},
    owns{true} {}

Sound::Sound(Sound&& source) : sound{source.sound}, owns{source.sound}
{
    source.sound = nullptr;
}

Sound::Sound(orxSOUND *source) : sound{source}, owns{false} {}

Sound& Sound::operator=(Sound&& source)
{
    if (owns && sound) orxSound_Delete(sound);

    sound = source.sound;
    owns = source.owns;

    source.sound = nullptr;

    return *this;
}

Sound::~Sound()
{
    if (owns && sound) orxSound_Delete(sound);
}

Sound::operator bool() const
{
    return sound != nullptr;
}

bool Sound::operator==(const Sound& other) const
{
    return sound && sound == other.sound;
}

bool Sound::operator!=(const Sound& other) const
{
    return sound && sound != other.sound;
}

orx::Status Sound::Init()
{
    return orxSound_Init();
}

void Sound::Setup() { orxSound_Setup(); }
void Sound::Exit() { orxSound_Exit(); }

Sound Sound::Create(std::nullptr_t)
{
    return orxSound_Create();
}

Sound Sound::Create(ConstString configID)
{
    return orxSound_CreateFromConfig(configID);
}

Sound Sound::Create(U32 channelNumber, U32 sampleRate, ConstString name)
{
    return orxSound_CreateWithEmptyStream(channelNumber, sampleRate, name);
}

SoundSystem::Sample
Sound::CreateSample(U32 channelNumber, U32 frameNumber, U32 sampleRate, ConstString name)
{
    return orxSound_CreateSample(channelNumber, frameNumber, sampleRate, name);
}

orx::Status Sound::Delete(Sound& sound)
{
    return orxSound_Delete(sound.sound);
}

orx::Status Sound::DeleteSample(ConstString sampleName)
{
    return orxSound_DeleteSample(sampleName);
}

orx::Status Sound::attenuation(Float a)
{
    return orxSound_SetAttenuation(sound, a);
}

Float Sound::attenuation() const
{
    return orxSound_GetAttenuation(sound);
}

Float Sound::duration() const
{
    return orxSound_GetDuration(sound);
}

orx::Status Sound::pitch(Float p)
{
    return orxSound_SetPitch(sound, p);
}

Float Sound::pitch() const
{
    return orxSound_GetPitch(sound);
}

orx::Status Sound::volume(Float v)
{
    return orxSound_SetVolume(sound, v);
}

Float Sound::volume() const
{
    return orxSound_GetVolume(sound);
}

ConstString Sound::name() const
{
    return orxSound_GetName(sound);
}

orx::Status Sound::position(const Vector& p)
{
    return orxSound_SetPosition(sound, &p.vector);
}

Vector Sound::position() const
{
    orxVECTOR result;
    return orxSound_GetPosition(sound, &result);
}

orx::Status Sound::referenceDistance(Float distance)
{
    return orxSound_SetReferenceDistance(sound, distance);
}

Float Sound::referenceDistance() const
{
    return orxSound_GetReferenceDistance(sound);
}

Sound::Status Sound::status() const
{
    return static_cast<Status>(orxSound_GetStatus(sound));
}

bool Sound::looping() const
{
    return orxSound_IsLooping(sound);
}

orx::Status Sound::loop(bool l)
{
    return orxSound_Loop(sound, l);
}

orx::Status Sound::pause()
{
    return orxSound_Pause(sound);
}

orx::Status Sound::play()
{
    return orxSound_Play(sound);
}

orx::Status Sound::stop()
{
    return orxSound_Stop(sound);
}

bool Sound::isStream()
{
    return orxSound_IsStream(sound);
}

orx::Status Sound::linkSample(ConstString sampleName)
{
    return orxSound_LinkSample(sound, sampleName);
}

orx::Status Sound::unlinkSample()
{
    return orxSound_UnlinkSample(sound);
}

SoundSystem::Sample Sound::GetSample(ConstString name)
{
    return orxSound_GetSample(name);
}

bool Sound::HasRecordingSupport()
{
    return orxSound_HasRecordingSupport();
}

orx::Status Sound::StartRecording(const char *filename, bool write,
                           U32 sampleRate, U32 channelNumber)
{
    return orxSound_StartRecording(filename, write, sampleRate, channelNumber);
}

orx::Status Sound::StopRecording()
{
    return orxSound_StopRecording();
}

}
