#include "sound/soundsystem.h"

#include "utils/string.h"
#include "math/vector.h"
#include "base/type.h"

namespace orx
{

// Sample
SoundSystem::Sample::Sample() : sample{nullptr}, owns{true} {}

SoundSystem::Sample::Sample(U32 channelNumber, U32 frameNumber, U32 sampleRate) :
    sample{orxSoundSystem_CreateSample(channelNumber, frameNumber, sampleRate)},
    owns{true} {}

SoundSystem::Sample::Sample(Sample&& source) : sample{source.sample}, owns{true}
{
    source.sample = nullptr;
}

SoundSystem::Sample::Sample(orxSOUNDSYSTEM_SAMPLE *source) :
    sample{source}, owns{false} {}

SoundSystem::Sample& SoundSystem::Sample::operator=(Sample&& source)
{
    if (owns && sample) orxSoundSystem_DeleteSample(sample);

    sample = source.sample;
    owns = source.owns;

    source.sample = nullptr;

    return *this;
}

SoundSystem::Sample::~Sample()
{
    if (owns && sample) orxSoundSystem_DeleteSample(sample);
}

SoundSystem::Sample::operator bool() const
{
    return sample != nullptr;
}

bool SoundSystem::Sample::operator==(const Sample& other)
{
    return sample && sample == other.sample;
}

bool SoundSystem::Sample::operator!=(const Sample& other)
{
    return sample && sample != other.sample;
}

SoundSystem::Sample
SoundSystem::Sample::Create(U32 channelNumber, U32 frameNumber, U32 sampleRate)
{
    return orxSoundSystem_CreateSample(channelNumber, frameNumber, sampleRate);
}

orx::Status SoundSystem::Sample::Delete(Sample& sample)
{
    orx::Status result = orxSoundSystem_DeleteSample(sample.sample);
    sample.sample = nullptr;

    return result;
}

orx::Status
SoundSystem::Sample::info(U32& channelNumber, U32& frameNumber, U32& sampleRate) const
{
    return orxSoundSystem_GetSampleInfo(sample, &channelNumber, &frameNumber, &sampleRate);
}

orx::Status SoundSystem::Sample::sampleData(const S16 *data, U32 sampleNumber)
{
    return orxSoundSystem_SetSampleData(sample, data, sampleNumber);
}

// Sound
SoundSystem::Sound::Sound() : sound{nullptr}, owns{true} {}

SoundSystem::Sound::Sound(const Sample& sample) :
    sound{orxSoundSystem_CreateFromSample(sample.sample)}, owns{true} {}

SoundSystem::Sound::Sound(U32 channelNumber, U32 sampleRate, ConstString reference) :
    sound{orxSoundSystem_CreateStream(channelNumber, sampleRate, reference)},
    owns{true} {}

SoundSystem::Sound::Sound(Sound&& source) : sound{source.sound}, owns{true}
{
    source.sound = nullptr;
}

SoundSystem::Sound::Sound(orxSOUNDSYSTEM_SOUND *source) : sound{source}, owns{false} {}

SoundSystem::Sound& SoundSystem::Sound::operator=(Sound&& source)
{
    if (owns && sound) orxSoundSystem_Delete(sound);

    sound = source.sound;
    owns = source.owns;

    source.sound = nullptr;

    return *this;
}

SoundSystem::Sound::~Sound()
{
    if (owns && sound) orxSoundSystem_Delete(sound);
}

SoundSystem::Sound::operator bool() const
{
    return sound != nullptr;
}

bool SoundSystem::Sound::operator==(const Sound& other)
{
    return sound && sound == other.sound;
}

bool SoundSystem::Sound::operator!=(const Sound& other)
{
    return sound && sound != other.sound;
}

SoundSystem::Sound SoundSystem::Sound::Create(const Sample& sample)
{
    return orxSoundSystem_CreateFromSample(sample.sample);
}

SoundSystem::Sound
SoundSystem::Sound::Create(U32 channelNumber, U32 sampleRate, ConstString reference)
{
    return orxSoundSystem_CreateStream(channelNumber, sampleRate, reference);
}

orx::Status SoundSystem::Sound::Delete(Sound& sound)
{
    orx::Status result =
        orxSoundSystem_Delete(sound.sound);
    sound.sound = nullptr;

    return result;
}

orx::Status SoundSystem::Sound::attenuation(Float a)
{
    return orxSoundSystem_SetAttenuation(sound, a);
}

Float SoundSystem::Sound::attenuation() const
{
    return orxSoundSystem_GetAttenuation(sound);
}

Float SoundSystem::Sound::duration() const
{
    return orxSoundSystem_GetDuration(sound);
}

orx::Status SoundSystem::Sound::pitch(Float p)
{
    return orxSoundSystem_SetPitch(sound, p);
}

Float SoundSystem::Sound::pitch() const
{
    return orxSoundSystem_GetPitch(sound);
}

orx::Status SoundSystem::Sound::volume(Float v)
{
    return orxSoundSystem_SetVolume(sound, v);
}

Float SoundSystem::Sound::volume() const
{
    return orxSoundSystem_GetVolume(sound);
}

orx::Status SoundSystem::Sound::position(const Vector& p)
{
    return orxSoundSystem_SetPosition(sound, &p.vector);
}

Vector SoundSystem::Sound::position() const
{
    orxVECTOR result;
    return orxSoundSystem_GetPosition(sound, &result);
}

orx::Status SoundSystem::Sound::referenceDistance(Float distance)
{
    return orxSoundSystem_SetReferenceDistance(sound, distance);
}

Float SoundSystem::Sound::referenceDistance() const
{
    return orxSoundSystem_GetReferenceDistance(sound);
}

SoundSystem::Status SoundSystem::Sound::status() const
{
    return static_cast<SoundSystem::Status>(
        orxSoundSystem_GetStatus(sound)
    );
}

bool SoundSystem::Sound::looping() const
{
    return orxSoundSystem_IsLooping(sound);
}

orx::Status SoundSystem::Sound::loop(bool l)
{
    return orxSoundSystem_Loop(sound, l);
}

orx::Status SoundSystem::Sound::pause() { return orxSoundSystem_Pause(sound); }
orx::Status SoundSystem::Sound::play() { return orxSoundSystem_Play(sound); }
orx::Status SoundSystem::Sound::stop() { return orxSoundSystem_Stop(sound); }

// Globals
orx::Status SoundSystem::Init()
{
    return orxSoundSystem_Init();
}

void SoundSystem::Setup() { orxSoundSystem_Setup(); }
void SoundSystem::Exit() { orxSoundSystem_Exit(); }

orx::Status SoundSystem::SetGlobalVolume(Float volume)
{
    return orxSoundSystem_SetGlobalVolume(volume);
}

Float SoundSystem::GetGlobalVolume()
{
    return orxSoundSystem_GetGlobalVolume();
}

orx::Status SoundSystem::SetListenerPosition(const Vector& position)
{
    return orxSoundSystem_SetListenerPosition(&position.vector);
}

Vector SoundSystem::GetListenerPosition()
{
    orxVECTOR result;
    return orxSoundSystem_GetListenerPosition(&result);
}

bool SoundSystem::HasRecordingSupport()
{
    return orxSoundSystem_HasRecordingSupport();
}

orx::Status
SoundSystem::StartRecording(const char *filename, bool write,
                            U32 sampleRate, U32 channelNumber)
{
    return orxSoundSystem_StartRecording(filename, write, sampleRate, channelNumber);
}

orx::Status SoundSystem::StopRecording()
{
    return orxSoundSystem_StopRecording();
}

}
