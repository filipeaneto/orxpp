#include "base/type.h"

namespace orx
{

Status::Status(orxSTATUS value) : status{value} {}

Status::Status(ID value) : status{static_cast<orxSTATUS>(value)} {}

bool Status::operator==(const Status& other) const
{
    return status == other.status;
}

bool Status::operator!=(const Status& other) const
{
    return status != other.status;
}

Status::operator bool() const { return status == orxSTATUS_SUCCESS; }

Status::operator orxSTATUS() const { return status; }

Status::operator ID() const { return static_cast<ID>(status); }

}
