#include <orx.h>

#include "anim/anim.h"

#include "object/structure.h"
#include "object/object.h"
#include "utils/string.h"
#include "core/event.h"
#include "base/type.h"

namespace orx
{

// CustomEvent
Anim::CustomEvent::CustomEvent(const orxANIM_CUSTOM_EVENT* source) :
    customEvent{source} {}

Anim::CustomEvent::operator bool() const
{
    return customEvent != nullptr;
}

bool Anim::CustomEvent::operator==(const CustomEvent& other) const
{
    return customEvent && customEvent == other.customEvent;
}

bool Anim::CustomEvent::operator!=(const CustomEvent& other) const
{
    return customEvent && customEvent != other.customEvent;
}

Float Anim::CustomEvent::timeStamp() const { return customEvent->fTimeStamp; }

Float Anim::CustomEvent::value() const { return customEvent->fValue; }

ConstString Anim::CustomEvent::name() const { return customEvent->zName; }

// Event
Anim::Event::Event(const orx::Event& source) : orx::Event{source} {}

Anim::Event::ID Anim::Event::id() const { return static_cast<ID>(orx::Event::id()); }

Float Anim::Event::customEventTime() const
{
    orxASSERT(id() == ID::CustomEvent);
    return static_cast<orxANIM_EVENT_PAYLOAD*>(payload())->fCustomEventTime;
}

Float Anim::Event::customEventValue() const
{
    orxASSERT(id() == ID::CustomEvent);
    return static_cast<orxANIM_EVENT_PAYLOAD*>(payload())->fCustomEventValue;
}

Anim Anim::Event::anim() const
{
    return static_cast<orxANIM_EVENT_PAYLOAD*>(payload())->pstAnim;
}

ConstString Anim::Event::animName() const
{
    return static_cast<orxANIM_EVENT_PAYLOAD*>(payload())->zAnimName;
}

ConstString Anim::Event::customEventName() const
{
    orxASSERT(id() == ID::CustomEvent);
    return static_cast<orxANIM_EVENT_PAYLOAD*>(payload())->zCustomEventName;
}

Object Anim::Event::recipient() const
{
    return orxOBJECT(orx::Event::recipient());
}

// Anim
Anim::Anim() : anim{nullptr}, owns{true} {}

Anim::Anim(orxANIM *source) : anim{source}, owns{false} {}

Anim::Anim(U32 flags, U32 keyNumber, U32 eventNumber) :
    anim{orxAnim_Create(flags, keyNumber, eventNumber)}, owns{true} {}

Anim::Anim(ConstString configID) :
    anim{orxAnim_CreateFromConfig(configID)}, owns{true} {}

Anim::Anim(Anim&& source) : anim{source.anim}, owns{source.owns}
{
    source.anim = nullptr;
}

Anim& Anim::operator=(Anim&& source)
{
    if (owns && anim) orxAnim_Delete(anim);

    anim = source.anim;
    owns = source.owns;

    source.anim = nullptr;

    return *this;
}

Anim::~Anim() { if (owns && anim) orxAnim_Delete(anim); }

Anim::Anim(const Structure& source) : anim{orxANIM(source.structure)}, owns{false} {}

Anim& Anim::operator=(const Structure& source)
{
    if (owns && anim) orxAnim_Delete(anim);

    anim = orxANIM(source.structure);
    owns = false;

    return *this;
}

Anim::operator Structure() const
{
    return orxSTRUCTURE(anim);
}

Anim::operator bool() const
{
    return anim != nullptr;
}

bool Anim::operator==(const Anim& other) const
{
    return anim && anim == other.anim;
}

bool Anim::operator!=(const Anim& other) const
{
    return anim && anim != other.anim;
}

void   Anim::Setup() { orxAnim_Setup(); }
Status Anim::Init() { return orxAnim_Init(); }
void   Anim::Exit() { orxAnim_Exit(); }

Anim Anim::Create(U32 flags, U32 keyNumber, U32 eventNumber)
{
    return orxAnim_Create(flags, keyNumber, eventNumber);
}

Anim Anim::Create(ConstString configID)
{
    return orxAnim_CreateFromConfig(configID);
}

Status Anim::Delete(Anim& anim)
{
    Status result = orxAnim_Delete(anim.anim);
    anim.anim = nullptr;

    return result;
}

Status Anim::addEvent(ConstString eventName, Float timeStamp, Float value)
{
    return orxAnim_AddEvent(anim, eventName, timeStamp, value);
}

Status Anim::addKey(Structure& data, Float timeStamp)
{
    return orxAnim_AddKey(anim, data.structure, timeStamp);
}

U32 Anim::eventCounter() const { return orxAnim_GetEventCounter(anim); }
U32 Anim::eventStorageSize() const { return orxAnim_GetEventStorageSize(anim); }
U32 Anim::keyCounter() const { return orxAnim_GetKeyCounter(anim); }

Structure Anim::keyData(U32 index) { return orxAnim_GetKeyData(anim, index); }

U32 Anim::keyStorageSize() const { return orxAnim_GetKeyStorageSize(anim); }

Float Anim::length() const { return orxAnim_GetLength(anim); }
ConstString Anim::name() const { return orxAnim_GetName(anim); }
Anim::CustomEvent Anim::nextEvent(Float timeStamp) const
{
    return orxAnim_GetNextEvent(anim, timeStamp);
}

void Anim::removeAllEvents() { orxAnim_RemoveAllEvents(anim); }
void Anim::removeAllKeys() { orxAnim_RemoveAllKeys(anim); }

Status Anim::removeLastEvent()
{
    return orxAnim_RemoveLastEvent(anim);
}

Status Anim::removeLastKey()
{
    return orxAnim_RemoveLastKey(anim);
}

Status Anim::update(Float timeStamp)
{
    return orxAnim_Update(anim, timeStamp, orxNULL);
}

Status Anim::update(Float timeStamp, U32& currentKey)
{
    return orxAnim_Update(anim, timeStamp, &currentKey);
}

}
