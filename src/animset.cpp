#include "anim/animset.h"

#include "object/structure.h"
#include "utils/string.h"
#include "anim/anim.h"

namespace orx
{

// LinkTable
AnimSet::LinkTable::LinkTable() : linkTable{nullptr} {}

AnimSet::LinkTable::~LinkTable()
{
    if (linkTable) orxAnimSet_DeleteLinkTable(linkTable);
}

AnimSet::LinkTable::LinkTable(orxANIMSET_LINK_TABLE *source) : linkTable{source} {}

// AnimSet
AnimSet::AnimSet() : animSet{nullptr}, owns{true} {}

AnimSet::AnimSet(U32 size) :
    animSet{orxAnimSet_Create(size)}, owns{true} {}

AnimSet::AnimSet(ConstString configID) :
    animSet{orxAnimSet_CreateFromConfig(configID)}, owns{true} {}

AnimSet::AnimSet(AnimSet&& source) : animSet{source.animSet}, owns{source.animSet}
{
    source.animSet = nullptr;
}

AnimSet::AnimSet(orxANIMSET *source) : animSet{source}, owns{false} {}

AnimSet& AnimSet::operator=(AnimSet&& source)
{
    if (owns && animSet) orxAnimSet_Delete(animSet);

    animSet = source.animSet;
    owns = source.owns;

    source.animSet = nullptr;

    return *this;
}

AnimSet::~AnimSet()
{
    if (owns && animSet) orxAnimSet_Delete(animSet);
}

AnimSet::AnimSet(const Structure& source) :
    animSet{orxANIMSET(source.structure)}, owns{false} {}

AnimSet& AnimSet::operator=(const Structure& source)
{
    if (owns && animSet) orxAnimSet_Delete(animSet);

    animSet = orxANIMSET(source.structure);
    owns = false;

    return *this;
}

AnimSet::operator Structure() const
{
    return orxSTRUCTURE(animSet);
}

AnimSet::operator bool() const
{
    return animSet != nullptr;
}

bool AnimSet::operator==(const AnimSet& other) const
{
    return animSet && animSet == other.animSet;
}

bool AnimSet::operator!=(const AnimSet& other) const
{
    return animSet && animSet != other.animSet;
}

Status AnimSet::Init()
{
    return orxAnimSet_Init();
}

void AnimSet::Setup() { orxAnimSet_Setup(); }
void AnimSet::Exit() { orxAnimSet_Exit(); }

AnimSet AnimSet::Create(U32 size)
{
    return orxAnimSet_Create(size);
}

AnimSet AnimSet::Create(ConstString configID)
{
    return orxAnimSet_CreateFromConfig(configID);
}

Status AnimSet::Delete(AnimSet& animSet)
{
    return orxAnimSet_Delete(animSet.animSet);
}

U32 AnimSet::addAnim(Anim& anim)
{
    return orxAnimSet_AddAnim(animSet, anim.anim);
}

Status AnimSet::removeAnim(U32 animID)
{
    return orxAnimSet_RemoveAnim(animSet, animID);
}

Status AnimSet::removeAllAnims()
{
    return orxAnimSet_RemoveAllAnims(animSet);
}

U32 AnimSet::addLink(U32 srcAnim, U32 dstAnim)
{
    return orxAnimSet_AddLink(animSet, srcAnim, dstAnim);
}

Status AnimSet::removeLink(U32 linkID)
{
    return orxAnimSet_RemoveLink(animSet, linkID);
}

void AnimSet::addReference() { orxAnimSet_AddReference(animSet); }
void AnimSet::removeReference() { orxAnimSet_RemoveReference(animSet); }

AnimSet::LinkTable AnimSet::cloneLinkTable() const
{
    return orxAnimSet_CloneLinkTable(animSet);
}

U32 AnimSet::computeAnim(U32 srcAnim, U32 dstAnim,
                         Float& time, LinkTable& linkTable,
                         bool& cut, bool& clearTarget)
{
    orxBOOL c, ct;
    U32 result = orxAnimSet_ComputeAnim(animSet, srcAnim, dstAnim, &time,
                                        linkTable.linkTable, &c, &ct);
    cut = c;
    clearTarget = ct;

    return result;
}

U32 AnimSet::findNextAnim(U32 srcAnim, U32 dstAnim)
{
    return orxAnimSet_FindNextAnim(animSet, srcAnim, dstAnim);
}

Anim AnimSet::anim(U32 animID) const
{
    return orxAnimSet_GetAnim(animSet, animID);
}

U32 AnimSet::animCounter() const
{
    return orxAnimSet_GetAnimCounter(animSet);
}

U32 AnimSet::animID(ConstString animName) const
{
    return orxAnimSet_GetAnimIDFromName(animSet, animName);
}

U32 AnimSet::animStorageSize() const
{
    return orxAnimSet_GetAnimStorageSize(animSet);
}

U32 AnimSet::link(U32 srcAnim, U32 dstAnim) const
{
    return orxAnimSet_GetLink(animSet, srcAnim, dstAnim);
}

Status AnimSet::linkProperty(U32 linkID, U32 property, U32 value)
{
    return orxAnimSet_SetLinkProperty(animSet, linkID, property, value);
}

U32 AnimSet::linkProperty(U32 linkID, U32 property) const
{
    return orxAnimSet_GetLinkProperty(animSet, linkID, property);
}

}
