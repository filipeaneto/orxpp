#include "utils/string.h"

#include "math/vector.h"

namespace orx
{

// iostream
std::ostream& operator<<(std::ostream& os, const ConstString& cs)
{
    return os << cs.string;
}

std::ostream& operator<<(std::ostream& os, const String& s)
{
    return os << s.string;
}

// ConstString
ConstString::operator bool() const
{
    return string != nullptr;
}

bool ConstString::operator==(ConstString other) const
{
    return orxString_Compare(string, other.string) == 0;
}

bool ConstString::operator!=(ConstString other) const
{
    return orxString_Compare(string, other.string) != 0;
}

bool ConstString::operator<(ConstString other) const
{
    return orxString_Compare(string, other.string) < 0;
}

ConstString::operator const orxSTRING() const { return string; }

U32 ConstString::continueCRC(U32 crc) const
{
    return orxString_ContinueCRC(string, crc);
}

U32 ConstString::continueCRC(U32 crc, U32 charNumber) const
{
    return orxString_NContinueCRC(string, crc, charNumber);
}

U32 ConstString::characterCounter() const
{
    return orxString_GetCharacterCounter(string);
}

U32 ConstString::length() const
{
    return orxString_GetLength(string);
}

ConstString ConstString::extension() const
{
    return orxString_GetExtension(string);
}

U32 ConstString::firstCharacterCodePoint(ConstString& remaining) const
{
    return orxString_GetFirstCharacterCodePoint(string, &remaining.string);
}

U32 ConstString::firstCharacterCodePoint() const
{
    return orxString_GetFirstCharacterCodePoint(string, orxNULL);
}

S32 ConstString::compare(ConstString other) const
{
    return orxString_Compare(string, other.string);
}

S32 ConstString::compare(ConstString other, U32 charNumber) const
{
    return orxString_NCompare(string, other.string, charNumber);
}

S32 ConstString::icompare(ConstString other) const
{
    return orxString_ICompare(string, other.string);
}

S32 ConstString::icompare(ConstString other, U32 charNumber) const
{
    return orxString_NICompare(string, other.string, charNumber);
}

ConstString ConstString::searchChar(Char c) const
{
    return orxString_SearchChar(string, c);
}

S32 ConstString::searchChar(Char c, U32 position) const
{
    return orxString_SearchCharIndex(string, c, position);
}

ConstString ConstString::searchString(ConstString string2) const
{
    return orxString_SearchString(string, string2.string);
}

ConstString ConstString::skipPath() const
{
    return orxString_SkipPath(string);
}

ConstString ConstString::skipWhiteSpaces() const
{
    return orxString_SkipWhiteSpaces(string);
}

U32 ConstString::toCRC() const
{
    return orxString_ToCRC(string);
}

U32 ConstString::toCRC(U32 charNumber) const
{
    return orxString_NToCRC(string, charNumber);
}

Status ConstString::toBool(bool& outValue, ConstString& remaining) const
{
    orxBOOL value{};
    orxSTATUS result = orxString_ToBool(string, &value, &remaining.string);

    outValue = value;

    return result;
}

Status ConstString::toBool(bool& outValue, std::nullptr_t) const
{
    orxBOOL value{};
    orxSTATUS result = orxString_ToBool(string, &value, orxNULL);

    outValue = value;

    return result;
}

bool ConstString::toBool() const
{
    orxBOOL value{};
    orxString_ToBool(string, &value, orxNULL);
    return value;
}

Status ConstString::toFloat(Float& outValue, ConstString& remaining) const
{
    return orxString_ToFloat(string, &outValue, &remaining.string);
}

Status ConstString::toFloat(Float& outValue, std::nullptr_t) const
{
    return orxString_ToFloat(string, &outValue, orxNULL);
}

Float ConstString::toFloat() const
{
    Float value;
    orxString_ToFloat(string, &value, orxNULL);
    return value;
}

Status ConstString::toS32(S32& outValue, ConstString& remaining) const
{
    return orxString_ToS32(string, &outValue, &remaining.string);
}

Status ConstString::toS32(S32& outValue, std::nullptr_t) const
{
    return orxString_ToS32(string, &outValue, orxNULL);
}

S32 ConstString::toS32() const
{
    S32 value;
    orxString_ToS32(string, &value, orxNULL);
    return value;
}

Status ConstString::toS32(S32& outValue, U32 base, ConstString& remaining) const
{
    return orxString_ToS32Base(string, base, &outValue, &remaining.string);
}

Status ConstString::toS32(S32& outValue, U32 base, std::nullptr_t) const
{
    return orxString_ToS32Base(string, base, &outValue, orxNULL);
}

S32 ConstString::toS32(U32 base) const
{
    S32 value;
    orxString_ToS32Base(string, base, &value, orxNULL);
    return value;
}

Status ConstString::toS64(S64& outValue, ConstString& remaining) const
{
    return orxString_ToS64(string, &outValue, &remaining.string);
}

Status ConstString::toS64(S64& outValue, std::nullptr_t) const
{
    return orxString_ToS64(string, &outValue, orxNULL);
}

S64 ConstString::toS64() const
{
    S64 value;
    orxString_ToS64(string, &value, orxNULL);
    return value;
}

Status ConstString::toS64(S64& outValue, U32 base, ConstString& remaining) const
{
    return orxString_ToS64Base(string, base, &outValue, &remaining.string);
}

Status ConstString::toS64(S64& outValue, U32 base, std::nullptr_t) const
{
    return orxString_ToS64Base(string, base, &outValue, orxNULL);
}

S64 ConstString::toS64(U32 base) const
{
    S64 value;
    orxString_ToS64Base(string, base, &value, orxNULL);
    return value;
}

Status ConstString::toU32(U32& outValue, ConstString& remaining) const
{
    return orxString_ToU32(string, &outValue, &remaining.string);
}

Status ConstString::toU32(U32& outValue, std::nullptr_t) const
{
    return orxString_ToU32(string, &outValue, orxNULL);
}

U32 ConstString::toU32() const
{
    U32 value;
    orxString_ToU32(string, &value, orxNULL);
    return value;
}

Status ConstString::toU32(U32& outValue, U32 base, ConstString& remaining) const
{
    return orxString_ToU32Base(string, base, &outValue, &remaining.string);
}

Status ConstString::toU32(U32& outValue, U32 base, std::nullptr_t) const
{
    return orxString_ToU32Base(string, base, &outValue, orxNULL);
}

U32 ConstString::toU32(U32 base) const
{
    U32 value;
    orxString_ToU32Base(string, base, &value, orxNULL);
    return value;
}

Status ConstString::toU64(U64& outValue, ConstString& remaining) const
{
    return orxString_ToU64(string, &outValue, &remaining.string);
}

Status ConstString::toU64(U64& outValue, std::nullptr_t) const
{
    return orxString_ToU64(string, &outValue, orxNULL);
}

U64 ConstString::toU64() const
{
    U64 value;
    orxString_ToU64(string, &value, orxNULL);
    return value;
}

Status ConstString::toU64(U64& outValue, U32 base, ConstString& remaining) const
{
    return orxString_ToU64Base(string, base, &outValue, &remaining.string);
}

Status ConstString::toU64(U64& outValue, U32 base, std::nullptr_t) const
{
    return orxString_ToU64Base(string, base, &outValue, orxNULL);
}

U64 ConstString::toU64(U32 base) const
{
    U64 value;
    orxString_ToU64Base(string, base, &value, orxNULL);
    return value;
}

Status ConstString::toVector(Vector& outValue, ConstString& remaining) const
{
    return orxString_ToVector(string, &outValue.vector, &remaining.string);
}

Status ConstString::toVector(Vector& outValue, std::nullptr_t) const
{
    return orxString_ToVector(string, &outValue.vector, orxNULL);
}

Vector ConstString::toVector() const
{
    Vector outValue;
    orxString_ToVector(string, &outValue.vector, orxNULL);
    return outValue;
}

// String
String::String() : string{} {}

String::String(ConstString source) : string{source.string} {}

String::String(ConstString source, U32 charNumber) :
    string{source.string, charNumber} {}

String& String::operator=(ConstString source)
{
    string = source.string;
    return *this;
}

String::operator bool() const
{
    return string.empty();
}

bool String::operator==(ConstString other) const
{
    return string == other.string;
}

bool String::operator!=(ConstString other) const
{
    return string != other.string;
}

bool String::operator<(ConstString other) const
{
    return string < other.string;
}

String::operator ConstString() const { return string.c_str(); }

Status String::Init() { return orxString_Init(); }
void String::Setup() { orxString_Setup(); }
void String::Exit() { orxString_Exit(); }


ConstString String::GetFromID(U32 id)
{
    return orxString_GetFromID(id);
}

U32 String::GetID(ConstString string)
{
    return orxString_GetID(string.string);
}

U32 String::GetUTF8CharacterLength(U32 characterCodePoint)
{
    return orxString_GetUTF8CharacterLength(characterCodePoint);
}

bool String::IsCharacterAlphaNumeric(U32 characterCodePoint)
{
    return orxString_IsCharacterAlphaNumeric(characterCodePoint);
}

bool String::IsCharacterASCII(U32 characterCodePoint)
{
    return orxString_IsCharacterASCII(characterCodePoint);
}

String& String::copy(ConstString source)
{
    string = source.string;
    return *this;
}

String& String::copy(ConstString source, U32 charNumber)
{
    string.assign(source.string, charNumber);
    return *this;
}

U32 String::print(U32 size, U32 characterCodePoint)
{
    char buffer[5];
    U32 result = orxString_PrintUTF8Character(buffer, size, characterCodePoint);
    string += buffer;

    return result;
}

String& String::lowerCase()
{
    size_t len = string.length();
    char *buffer = new char[len + 1];
    string.copy(buffer, len);
    buffer[len] = '\0';

    orxString_LowerCase(buffer);

    string = std::move(buffer);

    delete[] buffer;

    return *this;
}

String& String::upperCase()
{
    size_t len = string.length();
    char *buffer = new char[len + 1];
    string.copy(buffer, len);
    buffer[len] = '\0';

    orxString_UpperCase(buffer);

    string = std::move(buffer);

    delete[] buffer;

    return *this;
}

U32 String::continueCRC(U32 crc) const
{
    return orxString_ContinueCRC(string.c_str(), crc);
}

U32 String::continueCRC(U32 crc, U32 charNumber) const
{
    return orxString_NContinueCRC(string.c_str(), crc, charNumber);
}

U32 String::characterCounter() const
{
    return orxString_GetCharacterCounter(string.c_str());
}

U32 String::length() const
{
    return orxString_GetLength(string.c_str());
}

ConstString String::extension() const
{
    return orxString_GetExtension(string.c_str());
}

U32 String::firstCharacterCodePoint(ConstString& remaining) const
{
    return orxString_GetFirstCharacterCodePoint(string.c_str(), &remaining.string);
}

U32 String::firstCharacterCodePoint() const
{
    return orxString_GetFirstCharacterCodePoint(string.c_str(), orxNULL);
}

S32 String::compare(ConstString other) const
{
    return orxString_Compare(string.c_str(), other.string);
}

S32 String::compare(ConstString other, U32 charNumber) const
{
    return orxString_NCompare(string.c_str(), other.string, charNumber);
}

S32 String::icompare(ConstString other) const
{
    return orxString_ICompare(string.c_str(), other.string);
}

S32 String::icompare(ConstString other, U32 charNumber) const
{
    return orxString_NICompare(string.c_str(), other.string, charNumber);
}

ConstString String::searchChar(Char c) const
{
    return orxString_SearchChar(string.c_str(), c);
}

S32 String::searchChar(Char c, U32 position) const
{
    return orxString_SearchCharIndex(string.c_str(), c, position);
}

ConstString String::searchString(ConstString string2) const
{
    return orxString_SearchString(string.c_str(), string2.string);
}

ConstString String::skipPath() const
{
    return orxString_SkipPath(string.c_str());
}

ConstString String::skipWhiteSpaces() const
{
    return orxString_SkipWhiteSpaces(string.c_str());
}

U32 String::toCRC() const
{
    return orxString_ToCRC(string.c_str());
}

U32 String::toCRC(U32 charNumber) const
{
    return orxString_NToCRC(string.c_str(), charNumber);
}

Status String::toBool(bool& outValue, ConstString& remaining) const
{
    orxBOOL value{};
    orxSTATUS result = orxString_ToBool(string.c_str(), &value, &remaining.string);

    outValue = value;

    return result;
}

Status String::toBool(bool& outValue, std::nullptr_t) const
{
    orxBOOL value{};
    orxSTATUS result = orxString_ToBool(string.c_str(), &value, orxNULL);

    outValue = value;

    return result;
}

bool String::toBool() const
{
    orxBOOL value{};
    orxString_ToBool(string.c_str(), &value, orxNULL);
    return value;
}

Status String::toFloat(Float& outValue, ConstString& remaining) const
{
    return orxString_ToFloat(string.c_str(), &outValue, &remaining.string);
}

Status String::toFloat(Float& outValue, std::nullptr_t) const
{
    return orxString_ToFloat(string.c_str(), &outValue, orxNULL);
}

Float String::toFloat() const
{
    Float value;
    orxString_ToFloat(string.c_str(), &value, orxNULL);
    return value;
}

Status String::toS32(S32& outValue, ConstString& remaining) const
{
    return orxString_ToS32(string.c_str(), &outValue, &remaining.string);
}

Status String::toS32(S32& outValue, std::nullptr_t) const
{
    return orxString_ToS32(string.c_str(), &outValue, orxNULL);
}

S32 String::toS32() const
{
    S32 value;
    orxString_ToS32(string.c_str(), &value, orxNULL);
    return value;
}

Status String::toS32(S32& outValue, U32 base, ConstString& remaining) const
{
    return orxString_ToS32Base(string.c_str(), base, &outValue, &remaining.string);
}

Status String::toS32(S32& outValue, U32 base, std::nullptr_t) const
{
    return orxString_ToS32Base(string.c_str(), base, &outValue, orxNULL);
}

S32 String::toS32(U32 base) const
{
    S32 value;
    orxString_ToS32Base(string.c_str(), base, &value, orxNULL);
    return value;
}

Status String::toS64(S64& outValue, ConstString& remaining) const
{
    return orxString_ToS64(string.c_str(), &outValue, &remaining.string);
}

Status String::toS64(S64& outValue, std::nullptr_t) const
{
    return orxString_ToS64(string.c_str(), &outValue, orxNULL);
}

S64 String::toS64() const
{
    S64 value;
    orxString_ToS64(string.c_str(), &value, orxNULL);
    return value;
}

Status String::toS64(S64& outValue, U32 base, ConstString& remaining) const
{
    return orxString_ToS64Base(string.c_str(), base, &outValue, &remaining.string);
}

Status String::toS64(S64& outValue, U32 base, std::nullptr_t) const
{
    return orxString_ToS64Base(string.c_str(), base, &outValue, orxNULL);
}

S64 String::toS64(U32 base) const
{
    S64 value;
    orxString_ToS64Base(string.c_str(), base, &value, orxNULL);
    return value;
}

Status String::toU32(U32& outValue, ConstString& remaining) const
{
    return orxString_ToU32(string.c_str(), &outValue, &remaining.string);
}

Status String::toU32(U32& outValue, std::nullptr_t) const
{
    return orxString_ToU32(string.c_str(), &outValue, orxNULL);
}

U32 String::toU32() const
{
    U32 value;
    orxString_ToU32(string.c_str(), &value, orxNULL);
    return value;
}

Status String::toU32(U32& outValue, U32 base, ConstString& remaining) const
{
    return orxString_ToU32Base(string.c_str(), base, &outValue, &remaining.string);
}

Status String::toU32(U32& outValue, U32 base, std::nullptr_t) const
{
    return orxString_ToU32Base(string.c_str(), base, &outValue, orxNULL);
}

U32 String::toU32(U32 base) const
{
    U32 value;
    orxString_ToU32Base(string.c_str(), base, &value, orxNULL);
    return value;
}

Status String::toU64(U64& outValue, ConstString& remaining) const
{
    return orxString_ToU64(string.c_str(), &outValue, &remaining.string);
}

Status String::toU64(U64& outValue, std::nullptr_t) const
{
    return orxString_ToU64(string.c_str(), &outValue, orxNULL);
}

U64 String::toU64() const
{
    U64 value;
    orxString_ToU64(string.c_str(), &value, orxNULL);
    return value;
}

Status String::toU64(U64& outValue, U32 base, ConstString& remaining) const
{
    return orxString_ToU64Base(string.c_str(), base, &outValue, &remaining.string);
}

Status String::toU64(U64& outValue, U32 base, std::nullptr_t) const
{
    return orxString_ToU64Base(string.c_str(), base, &outValue, orxNULL);
}

U64 String::toU64(U32 base) const
{
    U64 value;
    orxString_ToU64Base(string.c_str(), base, &value, orxNULL);
    return value;
}

Status String::toVector(Vector& outValue, ConstString& remaining) const
{
    return orxString_ToVector(string.c_str(), &outValue.vector, &remaining.string);
}

Status String::toVector(Vector& outValue, std::nullptr_t) const
{
    return orxString_ToVector(string.c_str(), &outValue.vector, orxNULL);
}

Vector String::toVector() const
{
    Vector outValue;
    orxString_ToVector(string.c_str(), &outValue.vector, orxNULL);
    return outValue;
}

}
