#include "anim/animpointer.h"

#include "object/structure.h"
#include "anim/animset.h"
#include "utils/string.h"

namespace orx
{

AnimPointer::AnimPointer() : animPointer{nullptr}, owns{true} {}

AnimPointer::AnimPointer(const Structure& owner, AnimSet& animSet) :
    animPointer{orxAnimPointer_Create(owner.structure, animSet.animSet)},
    owns{true} {}

AnimPointer::AnimPointer(const Structure& owner, ConstString configID) :
    animPointer{orxAnimPointer_CreateFromConfig(owner.structure, configID)},
    owns{true} {}

AnimPointer::AnimPointer(AnimPointer&& source) : animPointer{source.animPointer}, owns{true}
{
    source.animPointer = nullptr;
}

AnimPointer::AnimPointer(orxANIMPOINTER *source) : animPointer{source}, owns{false} {}

AnimPointer& AnimPointer::operator=(AnimPointer&& source)
{
    if (owns && animPointer) orxAnimPointer_Delete(animPointer);

    animPointer = source.animPointer;
    owns = source.owns;

    source.animPointer = nullptr;

    return *this;
}

AnimPointer::~AnimPointer()
{
    if (owns && animPointer) orxAnimPointer_Delete(animPointer);
}

AnimPointer::AnimPointer(const Structure& source) :
    animPointer{orxANIMPOINTER(source.structure)}, owns{false} {}

AnimPointer& AnimPointer::operator=(const Structure& source)
{
    if (owns && animPointer) orxAnimPointer_Delete(animPointer);

    animPointer = orxANIMPOINTER(source.structure);
    owns = false;

    return *this;
}

AnimPointer::operator Structure() const
{
    return orxSTRUCTURE(animPointer);
}

AnimPointer::operator bool() const
{
    return animPointer != nullptr;
}

bool AnimPointer::operator==(const AnimPointer& other) const
{
    return animPointer && animPointer == other.animPointer;
}

bool AnimPointer::operator!=(const AnimPointer& other) const
{
    return animPointer && animPointer != other.animPointer;
}

Status AnimPointer::Init()
{
    return orxAnimPointer_Init();
}

void AnimPointer::Setup() { orxAnimPointer_Setup(); }
void AnimPointer::Exit() { orxAnimPointer_Exit(); }

AnimPointer AnimPointer::Create(const Structure& owner, AnimSet& animSet)
{
    return orxAnimPointer_Create(owner.structure, animSet.animSet);
}

AnimPointer AnimPointer::Create(const Structure& owner, ConstString configID)
{
    return orxAnimPointer_CreateFromConfig(owner.structure, configID);
}

Status AnimPointer::Delete(AnimPointer& animPointer)
{
    Status result = orxAnimPointer_Delete(animPointer.animPointer);
    animPointer.animPointer = nullptr;
    return result;
}

AnimSet AnimPointer::animSet() const
{
    return orxAnimPointer_GetAnimSet(animPointer);
}

Status AnimPointer::currentAnim(U32 animID)
{
    return orxAnimPointer_SetCurrentAnim(animPointer, animID);
}

Status AnimPointer::currentAnim(ConstString animName)
{
    return orxAnimPointer_SetCurrentAnimFromName(animPointer, animName);
}


U32 AnimPointer::currentAnim() const
{
    return orxAnimPointer_GetCurrentAnim(animPointer);
}


Structure AnimPointer::currentAnimData() const
{
    return orxAnimPointer_GetCurrentAnimData(animPointer);
}


ConstString AnimPointer::currentAnimName() const
{
    return orxAnimPointer_GetCurrentAnimName(animPointer);
}

Status AnimPointer::time(Float t)
{
    return orxAnimPointer_SetTime(animPointer, t);
}

Float AnimPointer::currentTime() const
{
    return orxAnimPointer_GetCurrentTime(animPointer);
}

Status AnimPointer::frequency(Float f)
{
    return orxAnimPointer_SetFrequency(animPointer, f);
}

Float AnimPointer::frequency() const
{
    return orxAnimPointer_GetFrequency(animPointer);
}

Structure AnimPointer::owner() const
{
    return orxAnimPointer_GetOwner(animPointer);
}

Status AnimPointer::targetAnim(U32 animID)
{
    return orxAnimPointer_SetTargetAnim(animPointer, animID);
}

Status AnimPointer::targetAnim(ConstString animName)
{
    return orxAnimPointer_SetTargetAnimFromName(animPointer, animName);
}

// XXX
//U32 AnimPointer::targetAnim() const
//{
//    return orxAnimPointer_GetTargetAnim(animPointer);
//}

ConstString AnimPointer::targetAnimName() const
{
    return orxAnimPointer_GetTargetAnimName(animPointer);
}

Status AnimPointer::pause(bool p)
{
    return orxAnimPointer_Pause(animPointer, p);
}

}
