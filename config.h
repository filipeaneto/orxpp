#ifndef ORXPP_CONFIG_H
#define ORXPP_CONFIG_H

#include "orx.h"

#include "orxpp/type.h"
#include "orxpp/string.h"
#include "orxpp/vector.h"

namespace orx {
namespace Config {

inline Status PushSection(ConstString sectionName)
{
    return static_cast<Status>(orxConfig_PushSection(sectionName));
}

inline Status PopSection()
{
    return static_cast<Status>(orxConfig_PopSection());
}

template<class F>
inline Status Section(ConstString sectionName, F f)
{
    if (PushSection(sectionName) == Status::Success) {
        f();
        PopSection();

        return Status::Success;
    }

    return Status::Failure;
}

inline U32 GetU32(ConstString key)
{
    return orxConfig_GetU32(key);
}

inline bool GetBool(ConstString key)
{
    return orxConfig_GetBool(key);
}

inline Vector GetVector(ConstString key)
{
    orxVECTOR result;
    return orxConfig_GetVector(key, &result);
}

inline Status Set(ConstString key, bool value)
{
    return static_cast<Status>(orxConfig_SetBool(key, value));
}

} // namespace Config
} // namespace orx

#endif // ORXPP_CONFIG_H
