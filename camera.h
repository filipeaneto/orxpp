#ifndef ORXPP_CAMERA_H
#define ORXPP_CAMERA_H

#include "orx.h"

#include "orxpp/structure.h"
#include "orxpp/spawner.h"
#include "orxpp/string.h"
#include "orxpp/vector.h"
#include "orxpp/frame.h"
#include "orxpp/aabox.h"
#include "orxpp/type.h"

namespace orx
{

class Viewport;

class Camera
{
public:
    // Constructors, destructor and assignment
    Camera();
    explicit Camera(U32 flags);
    explicit Camera(ConstString configID);
    Camera(const Camera& source) = delete;
    Camera(Camera&& source);

    Camera& operator=(const Camera& source) = delete;
    Camera& operator=(Camera&& source);

    virtual ~Camera();

    // Validity and equality check
    operator bool() const;
    bool operator==(const Camera& other) const;
    bool operator!=(const Camera& other) const;

    // Methods
    inline static Status Init();
    inline static void Setup();
    inline static void Exit();

    inline static Camera Create(U32 flags);
    inline static Camera Create(ConstString configID);

    inline static Status Delete(Camera& camera);

    inline static Camera Get(ConstString name);

    inline Status addGroupID(U32 groupID, bool addFirst = false);
    inline Status removeGroupID(U32 groupID);

    inline Frame frame() const;
    inline Status frustum(Float width, Float height, Float near, Float far);
    inline AABox frustum() const;

    inline U32 groupID() const;
    inline U32 groupIDCounter() const;

    inline ConstString name() const;

    inline Status parent(std::nullptr_t);
    inline Status parent(Object& p);
    inline Status parent(Spawner& p);
    inline Status parent(Camera& p);
    inline Status parent(Frame& p);
    inline Structure parent() const;

    inline Status position(const Vector& p);
    inline Vector position() const;
    inline Status rotation(Float r);
    inline Float rotation() const;
    inline Status zoom(Float z);
    inline Float zoom() const;

    // Friendship
    friend class Viewport;

protected:
    Camera(orxCAMERA *source);

private:
    orxCAMERA *camera;
    bool owns;
};

Camera::Camera() : camera{nullptr}, owns{true} {}

Camera::Camera(ConstString configID) :
    camera{orxCamera_CreateFromConfig(configID)}, owns{true} {}

Camera::Camera(Camera&& source) : camera{source.camera}, owns{source.owns}
{
    source.camera = nullptr;
}

Camera& Camera::operator=(Camera&& source)
{
    if (owns && camera) orxCamera_Delete(camera);

    camera = source.camera;
    owns = source.owns;

    source.camera = nullptr;

    return *this;
}

Camera::~Camera()
{
    if (owns && camera) orxCamera_Delete(camera);
}

Status Camera::rotation(Float r)
{
    return static_cast<Status>(orxCamera_SetRotation(camera, r));
}

Float Camera::rotation() const
{
    return orxCamera_GetRotation(camera);
}

Camera::Camera(orxCAMERA *source) : camera{source}, owns{false} {}

Status Camera::zoom(Float z)
{
    return static_cast<Status>(orxCamera_SetZoom(camera, z));
}

Float Camera::zoom() const
{
    return orxCamera_GetZoom(camera);
}

Status Camera::position(const Vector& p)
{
    return static_cast<Status>(orxCamera_SetPosition(camera, &p.vector));
}

Vector Camera::position() const
{
    orxVECTOR result;
    return orxCamera_GetPosition(camera, &result);
}

}

#endif // ORXPP_CAMERA_H
