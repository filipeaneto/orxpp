#ifndef ORXPP_PHYSICS_H
#define ORXPP_PHYSICS_H

#include "orx.h"

#include "orxpp/object.h"
#include "orxpp/vector.h"
#include "orxpp/event.h"

namespace orx {

namespace Physics {

struct Event
{
    static constexpr orx::Event::Type EventType{orx::Event::Type::Physics};

    enum class ID : U32
    {
        ContactAdd = orxPHYSICS_EVENT_CONTACT_ADD,
        ContactRemove,
        Number,
        None = orxENUM_NONE
    };

    ID id;
    mutable Object recipient, sender;

    // XXX keep private using friendship
    Event(const orxEVENT *event) :
        id{static_cast<ID>(event->eID)},
        recipient{orxOBJECT(event->hRecipient)},
        sender{orxOBJECT(event->hSender)}
    {
    }
};

inline Vector GetGravity()
{
    orxVECTOR result;
    return orxPhysics_GetGravity(&result);
}

inline Status SetGravity(const Vector& gravity)
{
    return static_cast<Status>(orxPhysics_SetGravity(&gravity.vector));
}

} // namespace Physics
} // namespace orx

#endif // ORXPP_PHYSICS_H
